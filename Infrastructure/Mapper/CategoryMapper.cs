﻿using AutoMapper;
using Infrastructure.Models.EfModels;
using System;

namespace Infrastructure.Mapper
{
    public class CategoryMapper : Profile
    {
        public CategoryMapper()
        {
            // Mapping Models Category -> Domain Category
            CreateMap<Models.Category, Domain.Category>()
                .ConstructUsing(x => new Domain.Category(new Guid(x.Id), x.Name))
                .ForAllMembers(s => s.Ignore());

            // Mapping Domain Category ->  Models Category
            CreateMap<Domain.Category, Models.Category>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id.ToString()))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));

            // Mapping Models Category -> Ef Models Category
            CreateMap<Models.Category, CategoryEf>()
                .ForMember(d => d.CateId, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Id, s => s.Ignore());

            // Mapping Ef Models  Category -> Models Category
            CreateMap<CategoryEf, Models.Category>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.CateId.ToString()))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
