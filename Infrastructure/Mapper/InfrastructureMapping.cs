﻿using AutoMapper;
using System;

namespace Infrastructure.Mapper
{
    public static class InfrastructureMapping
    {
        public static IMapper Mapper => _lazy.Value;

        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<PostMapper>();
                cfg.AddProfile<CategoryMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });
    }
}
