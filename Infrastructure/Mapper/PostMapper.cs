﻿using AutoMapper;
using Domain;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Mapper
{
    public class PostMapper : Profile
    {
        public PostMapper()
        {
            // Mapping from Post Aggregate Data Model -> Domain Post Aggregate
            CreateMap<PostAggregateDataModel, PostAggregate>()
                .ConstructUsing((source, destination) => {
                    var postAggregate = new PostAggregate(
                    new Guid(source.Id),
                    source.Title,
                    source.Thumbnail,
                    source.Content,
                    InfrastructureMapping.Mapper.Map<IEnumerable<Models.Category>, IEnumerable<Domain.Category>>(source.Categories),
                    new Author(new Guid(source.AuthorId), source.AuthorEmail)
                    );
                    postAggregate.UpdateCreateDate(source.CreatedAt);
                    postAggregate.UpdateLastModified(source.LastModifiedAt);
                    return postAggregate;
                })
                .ForAllOtherMembers(s => s.Ignore());

            // Mapping from Domain Post Aggregate ->  Post Aggregate Data Model
            CreateMap<PostAggregate, PostAggregateDataModel>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Title, s => s.MapFrom(src => src.Title.ToString()))
                .ForMember(d => d.Thumbnail, s => s.MapFrom(src => src.Thumbnail.ToString()))
                .ForMember(d => d.Content, s => s.MapFrom(src => src.Content.ToString()))
                .ForMember(d => d.CreatedAt, s => s.MapFrom(src => src.CreatedAt))
                .ForMember(d => d.LastModifiedAt, s => s.MapFrom(src => src.LastModifiedAt))
                .ForMember(d => d.Categories, s => s.MapFrom(src => src.Categories))
                .ForMember(d => d.AuthorId, s => s.MapFrom(src => src.Author.Id))
                .ForMember(d => d.DomainEvents, s => s.MapFrom((src, des, result) => {
                    result = src.DomainEvents.ToList();
                    src.ClearDomainEvents();
                    return result;
                }));

            // Mapping from Post Aggregate Data Model ->  PostEf
            CreateMap<PostAggregateDataModel, PostEf>()
               .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
               .ForMember(d => d.Title, s => s.MapFrom(src => src.Title.ToString()))
               .ForMember(d => d.Thumbnail, s => s.MapFrom(src => src.Thumbnail.ToString()))
               .ForMember(d => d.Content, s => s.MapFrom(src => src.Content.ToString()))
               .ForMember(d => d.CreateAt, s => s.MapFrom(src => src.CreatedAt))
               .ForMember(d => d.LastModifiedAt, s => s.MapFrom(src => src.LastModifiedAt))
               .ForMember(d => d.Categories, s => s.MapFrom(src => src.Categories))
               .ForMember(d => d.AuthorId, s => s.MapFrom(src => src.AuthorId))
               .ForMember(d => d.DomainEvents, s => s.MapFrom(src => src.DomainEvents))
               .ReverseMap();
        }
    }
}
