﻿using Infrastructure.Models;
using Nest;

namespace Infrastructure.IntegrationAppConfig
{
    public class ElasticsearchConfig : BaseConfig
    {
        public string Index { get; set; }

        public void AddPostMapping(IElasticClient esClient)
        {
            var response = esClient.Indices.Create(this.Index,
                index => index.Map<PostAggregateDataModel>(x => x
                    .Properties(p => p
                        .Keyword(k => k
                            .Name(id => id.Id))
                        .Text(t => t
                            .Name(title => title.Title))
                        .Text(t => t
                             .Name(c => c.Content))
                        .Text(t => t
                             .Name(th => th.Thumbnail))
                        .Keyword(t => t
                            .Name(auId => auId.AuthorId))
                        .Keyword(t => t
                            .Name(auId => auId.AuthorEmail))
                        .Object<Category>(o => o
                           .Name(n => n.Categories)
                           .Properties(pc => pc
                               .Keyword(k => k
                                   .Name(id => id.Id))
                               .Text(t => t
                                   .Name(n => n.Name))))
                        )
                ));
        }
    }
}
