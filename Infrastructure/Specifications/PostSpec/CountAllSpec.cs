﻿using Ardalis.Specification;
using Infrastructure.Models.EfModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Specifications.PostSpec
{
    public class CountAllSpec : Specification<PostEf>, ISingleResultSpecification
    {
        public CountAllSpec(Guid authorId)
        {
            Query.Where(x => x.AuthorId == authorId);
        }
    }
}
