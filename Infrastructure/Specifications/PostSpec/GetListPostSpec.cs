﻿using Ardalis.Specification;
using Infrastructure.Models.EfModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Specifications.PostSpec
{
    public class GetListPostSpec : Specification<PostEf>
    {
        public GetListPostSpec(Guid authorId, int offset, int limit)
        {
            Query.Where(x => x.AuthorId == authorId)
                 .Skip(offset)
                 .Take(limit)
                 .Include(c => c.Categories);
        }
    }
}
