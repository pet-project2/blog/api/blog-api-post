﻿using Ardalis.Specification;
using Infrastructure.Models.EfModels;
using System;
using System.Linq;

namespace Infrastructure.Specifications.PostSpec
{
    public class PostByIdAndAuthorIdSpec : Specification<PostEf>, ISingleResultSpecification
    {
        public PostByIdAndAuthorIdSpec(string id, string authorId)
        {
            var postId = new Guid(id);
            var guidAuthorId = new Guid(authorId);
            Query.Where(x => x.Id == postId &&
                        x.AuthorId == guidAuthorId)
                 .Include(c => c.Categories);
        }
    }
}
