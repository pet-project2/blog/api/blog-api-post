﻿using Ardalis.Specification;
using Infrastructure.Models.EfModels;
using System;

namespace Infrastructure.Specifications.PostSpec
{
    public class PostsByAuthorIdSpec : Specification<PostEf>
    {
        public PostsByAuthorIdSpec(string authorId)
        {
            var guidAuthorId = new Guid(authorId);
            Query.Where(x => x.AuthorId == guidAuthorId)
                       .Include(category => category.Categories);
        }
    }
}
