﻿using Ardalis.Specification;
using Infrastructure.Models.EfModels;
using System;
using System.Linq;

namespace Infrastructure.Specifications.PostSpec
{
    public class PostByIdSpec : Specification<PostEf>, ISingleResultSpecification
    {
        public PostByIdSpec(string postId)
        {
            var guidId = new Guid(postId);
            Query.Where(post => post.Id == guidId)
                 .Include(category => category.Categories);
        }
    }
}
