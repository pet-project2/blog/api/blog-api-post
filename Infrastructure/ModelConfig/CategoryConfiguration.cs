﻿using Infrastructure.Models.EfModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.ModelConfig
{
    public class CategoryConfiguration : IEntityTypeConfiguration<CategoryEf>
    {
        public void Configure(EntityTypeBuilder<CategoryEf> builder)
        {
            builder.ToTable("Category");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.CateId);

            builder.Property(x => x.Name);

            builder.Ignore(x => x.DomainEvents);

            builder.HasOne(x => x.Post)
                .WithMany(x => x.Categories)
                .HasForeignKey(x => x.PostId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
