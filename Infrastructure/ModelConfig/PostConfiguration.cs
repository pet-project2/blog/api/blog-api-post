﻿using Infrastructure.Models.EfModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.ModelConfig
{
    public class PostConfiguration : IEntityTypeConfiguration<PostEf>
    {
        public void Configure(EntityTypeBuilder<PostEf> builder)
        {
            builder.ToTable("Post");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Title);

            builder.Property(x => x.Content);

            builder.Property(x => x.Thumbnail);

            builder.Property(x => x.CreateAt);

            builder.Property(x => x.LastModifiedAt);

            builder.Property(x => x.AuthorId);

            builder.Property(x => x.AuthorEmail);

            builder.Ignore(x => x.DomainEvents);
        }
    }
}
