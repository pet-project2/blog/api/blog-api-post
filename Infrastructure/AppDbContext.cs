﻿using Infrastructure.Extensions;
using Infrastructure.ModelConfig;
using Infrastructure.Models.EfModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class AppDbContext : DbContext
    {
        private readonly IMediator _mediator;
        public AppDbContext(DbContextOptions<AppDbContext> options, IMediator mediator) : base(options)
        {
            this._mediator = mediator;
        }

        public virtual DbSet<PostEf> Posts { get; set; }
        public virtual DbSet<CategoryEf> Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PostConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            int result = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            if (this._mediator == null) return result;

            //this._logger.LogInformation("Starting publish message");
            await this._mediator.DispatchDomainEventsAsync(this);
            return result;
        }
    }
}
