﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models.EfModels
{
    public class PostEf : BaseModelEf
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public Guid AuthorId { get; set; }
        public string AuthorEmail { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime LastModifiedAt { get; set; }
        public ICollection<CategoryEf> Categories { get; set; }
    }
}
