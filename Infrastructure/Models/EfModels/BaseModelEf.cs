﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Models.EfModels
{
    public class BaseModelEf
    {
        public Guid Id { get; set; }
        public IReadOnlyCollection<INotification> DomainEvents { get; set; }
    }
}
