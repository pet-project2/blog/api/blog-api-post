﻿using System;

namespace Infrastructure.Models.EfModels
{
    public class CategoryEf : BaseModelEf
    {
        public Guid CateId { get; set; }
        public string Name { get; set; }
        public Guid PostId { get; set; }
        public PostEf Post { get; set; }
    }
}
