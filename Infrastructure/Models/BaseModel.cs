﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Models
{
    public class BaseModel
    {
        public string Id { get; set; }
        public IReadOnlyCollection<INotification> DomainEvents { get; set; }
    }
}
