﻿using Fury.Core.Interface;
using MediatR;
using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public class PostAggregateDataModel : BaseModel, IAggregateRoot
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string AuthorId { get; set; }
        public string AuthorEmail { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastModifiedAt { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}
