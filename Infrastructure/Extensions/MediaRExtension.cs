﻿using Infrastructure.Models.EfModels;
using MediatR;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Extensions
{
    internal static class MediaRExtension
    {
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, AppDbContext context)
        {
            var domainEntities = context.ChangeTracker.Entries<BaseModelEf>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

            var domainEvents = domainEntities.SelectMany(x => x.Entity.DomainEvents).ToList();

            foreach (var domainEvent in domainEvents)
            {
               await mediator.Publish(domainEvent);
            }
        }
    }
}
