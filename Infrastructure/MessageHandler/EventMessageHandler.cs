﻿using Confluent.Kafka;
using Domain.IntegrationEvents;
using Fury.Core.Interface;
using Infrastructure.IntegrationAppConfig;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.MessageHandler
{
    public class EventMessageHandler : IMessagePublisher<PostIntegrationEvent>
    {
        private readonly IProducer<string, string> _producer;
        private readonly string _topic;
        //private ILogger<EventMessageHandler> _logger;

        public EventMessageHandler(IProducer<string, string> producer, IOptions<KafkaProducerConfig> kafkaProducerConfig)
        {
            this._producer = producer;
            this._topic = kafkaProducerConfig.Value.Topic;
            //this._logger = logger;
        }
        public async Task Publish(PostIntegrationEvent messageEvent)
        {
            var jsonMessage = JsonSerializer.Serialize(messageEvent, new JsonSerializerOptions()
            {
               PropertyNamingPolicy = null
            });

            var message = new Message<string, string>
            {
                Key = messageEvent.Head.EventId,
                Value = jsonMessage
            };
            await this._producer.ProduceAsync(this._topic, message).ConfigureAwait(false);
            //this._logger.LogInformation($"Message is published to kafka {message} with id {messageEvent.Head.EventId}");
        }
    }
}
