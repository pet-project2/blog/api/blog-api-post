﻿using Infrastructure.Repository.CacheRepository;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Repository.SearchRepository;
using Infrastructure.Repository.EfRepository;
using Infrastructure.Specifications.PostSpec;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using Infrastructure.Mapper;
using System.Linq;

namespace Infrastructure.Repository
{
    public class ReadPostRepository : IReadPostRepository<PostAggregateDataModel>
    {
        private readonly ICacheRepository<PostAggregateDataModel> _cacheRepository;
        private readonly ISearchRepository<PostAggregateDataModel> _searchRepository;
        private readonly IEfRepository<PostEf, PostAggregateDataModel> _efRepository;
        public ReadPostRepository(ICacheRepository<PostAggregateDataModel> cacheRepository,
            ISearchRepository<PostAggregateDataModel> searchRepository,
            IEfRepository<PostEf, PostAggregateDataModel> efRepository)
        {
            this._cacheRepository = cacheRepository;
            this._searchRepository = searchRepository;
            this._efRepository = efRepository;
        }
        public async Task<PostAggregateDataModel> GetByIdAsync(string id, CancellationToken cancellationToken = default)
        {
            var cachKeys = $"post_{id}";
            var cacheValue = await this._cacheRepository.GetByKeyAsync(cachKeys);
            if (cacheValue != null)
            {
                return cacheValue;
            }
            var searchValue = await this._searchRepository.GetByIdAsync(id);
            if (searchValue != null)
            {
                await this._cacheRepository.SetCacheAsync(cachKeys, searchValue);
                return searchValue;
            }

            var spec = new PostByIdSpec(id);
            var efModel = await this._efRepository.GetBySpecAsync(spec, cancellationToken); // replace by mongoDb
            var postAggregate = InfrastructureMapping.Mapper.Map<PostAggregateDataModel>(efModel);
            return postAggregate;
        }

        public async Task<PostAggregateDataModel> GetByIdWithAuthorIdAsync(string id, string authorId, CancellationToken cancellationToken = default)
        {
            var cacheKey = $"post_{id}_author_{authorId}";
            var cacheValue = await this._cacheRepository.GetByKeyAsync(cacheKey);
            if (cacheValue != null)
            {
                return cacheValue;
            }
            var searchValue = await this._searchRepository.GetByIdWithAuthorIdAsync(id, authorId);
            if (searchValue != null)
            {
                return searchValue;
            }

            var spec = new PostByIdAndAuthorIdSpec(id, authorId);
            var efModel =  await this._efRepository.GetBySpecAsync(spec, cancellationToken); // replace by mongoDb
            var postAggregate = InfrastructureMapping.Mapper.Map<PostAggregateDataModel>(efModel);
            return postAggregate;
        }

        public async Task<IEnumerable<PostAggregateDataModel>> GetListAsync(CancellationToken cancellationToken = default)
        {
            return await Task.FromResult(new List<PostAggregateDataModel>() { });
        }

        public async Task<IEnumerable<PostAggregateDataModel>> GetAllByAuthorIdAsync(string authorId, CancellationToken cancellationToken = default)
        {
            var cacheKey = $"posts_all_list_{authorId}";
            var cacheValue = await this._cacheRepository.GetListByKeyAsync(cacheKey);
            if (cacheValue != null)
            {
                return cacheValue;
            }
            var searchValue = await this._searchRepository.GetAllByAuthorIdAsync(authorId);
            if (searchValue != null)
            {
                return searchValue;
            }

            var spec = new PostsByAuthorIdSpec(authorId);
            var efModel =  await this._efRepository.ListAsync(spec, cancellationToken); // replace by mongoDb
            var listPostAggregate = InfrastructureMapping.Mapper.Map<IEnumerable<PostAggregateDataModel>>(efModel);
            return listPostAggregate;
        }

        public async Task<(int, IEnumerable<PostAggregateDataModel>)> GetListByAuthorIdAsync(string authorId, int offset, int limit, CancellationToken cancellationToken = default)
        {
            var cacheKey = $"posts_list_paging_{authorId}";
            var searchValue = await this._searchRepository.GetListByAuthorIdAsync(authorId, offset, limit);
            if (searchValue.Item1 > 0)
            {
                await this._cacheRepository.SetCacheListItemAsync(cacheKey, searchValue.Item2);
                return searchValue;
            }

            var efResults = await this._efRepository.GetListByAuthorIdAsync(authorId, offset, limit, cancellationToken);
            if(efResults.Item1 > 0)
            {
                await this._cacheRepository.SetCacheListItemAsync(cacheKey, searchValue.Item2);
                return efResults;
            }

            return (0, Enumerable.Empty<PostAggregateDataModel>());
        }
    }
}
