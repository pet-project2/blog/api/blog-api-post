﻿using Infrastructure.Mapper;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using Infrastructure.Repository.EfRepository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class WritePostRepository : IWritePostRepository<PostAggregateDataModel>
    {
        private readonly IEfRepository<PostEf, PostAggregateDataModel> _efRepository;
        public WritePostRepository(IEfRepository<PostEf, PostAggregateDataModel> efRepository)
        {
            this._efRepository = efRepository;
        }

        public async Task AddAsync(PostAggregateDataModel entity, CancellationToken cancellationToken = default)
        {
            var postEf = InfrastructureMapping.Mapper.Map<PostEf>(entity);
            await this._efRepository.AddNoReturnAsync(postEf, cancellationToken);
        }

        public Task DeleteAsync(PostAggregateDataModel entity, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task DeleteRangeAsync(IEnumerable<PostAggregateDataModel> entities, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(PostAggregateDataModel entity, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(PostAggregateDataModel oldEntity, PostAggregateDataModel newEntity, CancellationToken cancellationToken = default)
        {
            var oldPostEf = InfrastructureMapping.Mapper.Map<PostEf>(oldEntity);
            var newPostEf = InfrastructureMapping.Mapper.Map<PostEf>(newEntity);
            await this._efRepository.UpdateWithNewEntityAsync(oldPostEf, newPostEf, cancellationToken);
        }
    }
}
