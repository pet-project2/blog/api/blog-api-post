﻿using Fury.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public interface IReadPostRepository<T> where T : class, IAggregateRoot
    {
        Task<T> GetByIdAsync(string id, CancellationToken cancellationToken = default);
        Task<T> GetByIdWithAuthorIdAsync(string id, string authorId, CancellationToken cancellationToken = default);
        Task<IEnumerable<T>> GetListAsync(CancellationToken cancellationToken = default);
        Task<IEnumerable<T>> GetAllByAuthorIdAsync(string authorId, CancellationToken cancellationToken = default);

        /// <summary>
        /// Get list post with paging by authorId
        /// </summary>
        /// <param name="authorId">Author id</param>
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Return total records and list data paging</returns>
        Task<(int, IEnumerable<T>)> GetListByAuthorIdAsync(string authorId, int offset, int limit, CancellationToken cancellationToken = default);
    }
}
