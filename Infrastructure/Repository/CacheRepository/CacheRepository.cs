﻿using Fury.Core.Interface;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.Models;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository.CacheRepository
{
    public class CacheRepository<T> : ICacheRepository<T> where T : class, IAggregateRoot
    {
        private readonly IDistributedCache _distributedCache;
        private readonly DistributedCacheEntryOptions _options;
        private readonly ILogger<CacheRepository<T>> _logger;

        public CacheRepository(IDistributedCache distributedCache, IOptions<RedisConfig> cacheOptions, ILogger<CacheRepository<T>> logger)
        {
            this._distributedCache = distributedCache;
            var absoluteExpiredTime = int.Parse(cacheOptions.Value.AbsoluteExpiredTime);
            var slidingExpiredTime = int.Parse(cacheOptions.Value.SlidingExpiredTime);
            this._options = new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(absoluteExpiredTime),
                SlidingExpiration = TimeSpan.FromSeconds(slidingExpiredTime)
            };

            this._logger = logger;
        }
        public async Task<T> GetByKeyAsync(string key)
        {
            try
            {
                var byteValue = await this._distributedCache.GetAsync(key);
                if (byteValue != null && byteValue.Length > 0)
                {
                    var stringValue = Encoding.UTF8.GetString(byteValue);
                    var value = JsonConvert.DeserializeObject<T>(stringValue);
                    return value;
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<IEnumerable<T>> GetListByKeyAsync(string key)
        {
            try
            {
                var byteValue = await this._distributedCache.GetAsync(key);
                if (byteValue != null && byteValue.Length > 0)
                {
                    var stringValue = Encoding.UTF8.GetString(byteValue);
                    var value = JsonConvert.DeserializeObject<IEnumerable<T>>(stringValue);
                    return value;
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task SetCacheAsync(string key, T entity)
        {
            try
            {
                var jsonStringValue = JsonConvert.SerializeObject(entity);
                var valueEncode = Encoding.UTF8.GetBytes(jsonStringValue);
                await this._distributedCache.SetAsync(key, valueEncode, this._options);
            }
            catch (Exception)
            {

            }
        }

        public async Task SetCacheListItemAsync(string key, IEnumerable<T> entity)
        {
            try
            {
                var jsonStringValue = JsonConvert.SerializeObject(entity);
                var valueEncode = Encoding.UTF8.GetBytes(jsonStringValue);
                await this._distributedCache.SetAsync(key, valueEncode, this._options);
            }
            catch (Exception)
            {

            }
        }
    }
}
