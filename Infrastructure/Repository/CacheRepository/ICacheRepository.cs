﻿using Fury.Core.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Repository.CacheRepository
{
    public interface ICacheRepository<T> where T : class, IAggregateRoot
    {
        Task<IEnumerable<T>> GetListByKeyAsync(string key);
        Task<T> GetByKeyAsync(string key);
        Task SetCacheAsync(string key, T entity);
        Task SetCacheListItemAsync(string key, IEnumerable<T> entity);
    }
}
