﻿using Fury.Core.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Repository.SearchRepository
{
    public interface ISearchRepository<T> where T : class, IAggregateRoot
    {
        Task<T> GetByIdAsync(string id);
        Task<T> GetByIdWithAuthorIdAsync(string id, string authorId);
        Task<IEnumerable<T>> GetAllByAuthorIdAsync(string authorId);
        Task<(int, IEnumerable<T>)> GetListByAuthorIdAsync(string authorId, int offset, int limit);
    }
}
