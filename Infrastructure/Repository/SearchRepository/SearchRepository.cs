﻿using Infrastructure.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repository.SearchRepository
{
    public class SearchRepository : ISearchRepository<PostAggregateDataModel>
    {
        private readonly IElasticClient _elasticClient;
        public SearchRepository(IElasticClient elasticClient)
        {
            this._elasticClient = elasticClient;
        }

        public async Task<IEnumerable<PostAggregateDataModel>> GetAllByAuthorIdAsync(string authorId)
        {
            if (string.IsNullOrEmpty(authorId))
                return null;

            var searchResult = await this._elasticClient.SearchAsync<PostAggregateDataModel>(s =>
                                                            s.Query(q => q.Term(p => p.AuthorId.Suffix("keyword"), authorId)
                                                        )
                                                    );

            return searchResult.Documents;

        }

        public async Task<PostAggregateDataModel> GetByIdAsync(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return null;

                var getResponse = await this._elasticClient.GetAsync<PostAggregateDataModel>(id);
                return getResponse.Source;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<PostAggregateDataModel> GetByIdWithAuthorIdAsync(string id, string authorId)
        {
            try
            {
                if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(authorId))
                    return null;

                var searchResult = await this._elasticClient.SearchAsync<PostAggregateDataModel>(s =>
                                            s.Query(q => q.Term(p => p.Id, id) && q.Term(p => p.AuthorId.Suffix("keyword"), authorId)
                                          )
                                        );

                if(searchResult.Documents != null)
                {
                    return searchResult.Documents.FirstOrDefault();
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<(int, IEnumerable<PostAggregateDataModel>)> GetListByAuthorIdAsync(string authorId, int offset, int limit)
        {
            if (string.IsNullOrEmpty(authorId))
                return (0, Enumerable.Empty<PostAggregateDataModel>());

            var searchResult = await this._elasticClient.SearchAsync<PostAggregateDataModel>(s =>
                                                            s.From(offset)
                                                            .Size(limit)
                                                            .Query(q => q.Term(p => p.AuthorId.Suffix("keyword"), authorId)
                                                        )
                                                    );
            return (Convert.ToInt32(searchResult.Total), searchResult.Documents);
        }
    }
}
