﻿using Ardalis.Specification.EntityFrameworkCore;
using Infrastructure.Mapper;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using Infrastructure.Specifications.PostSpec;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repository.EfRepository
{
    public class EfRepository : RepositoryBase<PostEf>, IEfRepository<PostEf, PostAggregateDataModel>
    {
        private readonly AppDbContext _dbContext;
        public EfRepository(AppDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task AddNoReturnAsync(PostEf entity, CancellationToken cancellationToken = default)
        {
            await this._dbContext.Posts.AddAsync(entity);
            await this._dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<(int, IEnumerable<PostAggregateDataModel>)> GetListByAuthorIdAsync(string authorId, int offset, int limit, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(authorId) && !Guid.TryParse(authorId, out _))
                return (0, Enumerable.Empty<PostAggregateDataModel>());

            var guidAuthorId = new Guid(authorId);
            var spec = new GetListPostSpec(guidAuthorId, offset, limit);
            var listPosts = await this.ListAsync(spec, cancellationToken); // replace by mongoDb

            var countSpec = new CountAllSpec(guidAuthorId);
            var total = await this.CountAsync(countSpec);

            var listPostAggregate = InfrastructureMapping.Mapper.Map<IEnumerable<PostAggregateDataModel>>(listPosts);
            return (total, listPostAggregate);
        }

        public async Task UpdateWithNewEntityAsync(PostEf oldEntity, PostEf newEntity, CancellationToken cancellationToken = default)
        {
            if (oldEntity == null)
            {
                string errMessage = "Can not update when couldn't found old post";
                throw new DbUpdateException(errMessage);
            }

            oldEntity.DomainEvents = newEntity.DomainEvents; // set domain event changed to old Entity

            // delete old category
            foreach (var oldCategory in oldEntity.Categories.ToList())
            {
                if (!newEntity.Categories.Any(c => c.CateId == oldCategory.CateId))
                    _dbContext.Categories.Remove(oldCategory);
            }

            foreach (var newCategory in newEntity.Categories)
            {
                var oldCategory = oldEntity.Categories.Where(c => c.CateId == newCategory.CateId).SingleOrDefault();

                if (oldCategory == null)
                {
                    oldEntity.Categories.Add(newCategory);
                    // mark this category is added to db
                    this._dbContext.Entry(newCategory).State = EntityState.Added;
                }
            }

            this._dbContext.Entry(oldEntity).CurrentValues.SetValues(newEntity);

            await this._dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
