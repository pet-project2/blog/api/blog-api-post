﻿using Ardalis.Specification;
using Fury.Core.Interface;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repository.EfRepository
{
    public interface IEfRepository<T, TResult> : IRepositoryBase<T>, IReadRepositoryBase<T> where T : class
    {
        Task AddNoReturnAsync(T entity, CancellationToken cancellationToken = default);

        Task UpdateWithNewEntityAsync(T oldEntity, T newEntity, CancellationToken cancellationToken = default);
        Task<(int, IEnumerable<TResult>)> GetListByAuthorIdAsync(string authorId, int offset, int limit, CancellationToken cancellationToken = default);

    }
}
