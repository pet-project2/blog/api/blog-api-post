﻿using Domain.IntegrationEvents;
using Elasticsearch.Net;
using Fury.Core.Interface;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.MessageHandler;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.IntegrationTest.Helper
{
    public class ConfigHelper
    {
        public static void ConfigElasticSearch(IServiceCollection services, string pathFileJson)
        {
            var configuration = new ConfigurationBuilder()
                                     .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                     .AddJsonFile("appsettings.json")
                                     .AddEnvironmentVariables()
                                     .Build();

            var stringJson = string.Empty;
            try
            {
                stringJson = File.ReadAllText(pathFileJson);
            }
            catch (Exception)
            {
                stringJson = File.ReadAllText("FakeData/FakeElasticsearchData/Fake_Error_Reponse.json");
            }

            var byteData = Encoding.UTF8.GetBytes(stringJson);
            var esSetting = configuration.GetSection("Elasticsearch").Get<ElasticsearchConfig>();
            var connectionPool = new SingleNodeConnectionPool(new Uri(esSetting.Uri));
            var settings = new ConnectionSettings(connectionPool, new InMemoryConnection(byteData))
                           .DefaultIndex(esSetting.Index)
                           .EnableDebugMode()
                           .OnRequestCompleted(response =>
                           {
                               // log out the request
                               if (response.RequestBodyInBytes != null)
                               {
                                   Debug.WriteLine(
                                       $"{response.HttpMethod} {response.Uri} \n" +
                                       $"{Encoding.UTF8.GetString(response.RequestBodyInBytes)}");
                               }
                               else
                               {
                                   Debug.WriteLine($"{response.HttpMethod} {response.Uri}");
                               }

                               Debug.WriteLine("-------");
                               // log out the response
                               if (response.ResponseBodyInBytes != null)
                               {
                                   Debug.WriteLine($"Status: {response.HttpStatusCode}\n" +
                                            $"{Encoding.UTF8.GetString(response.ResponseBodyInBytes)}\n" +
                                            $"{new string('-', 30)}\n");
                               }
                               else
                               {
                                   Debug.WriteLine($"Status: {response.HttpStatusCode}\n" +
                                            $"{new string('-', 30)}\n");
                               }

                           });

            var client = new ElasticClient(settings);
            esSetting.AddPostMapping(client);
            services.AddSingleton<IElasticClient>(client);
            services.AddScoped<IMessagePublisher<PostIntegrationEvent>, EventMessageHandler>();
        }
    }
}
