﻿using Confluent.Kafka;
using Domain.IntegrationEvents;
using DotNet.Testcontainers.Containers.Builders;
using DotNet.Testcontainers.Containers.Configurations.MessageBrokers;
using DotNet.Testcontainers.Containers.Modules.MessageBrokers;
using FluentAssertions;
using Fury.Core.Interface;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.MessageHandler;
using Infrastructure.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Api.IntegrationTest.InfrastructureTest.MessageHandlerTest
{
    public class MessageHandler_Spec : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly IProducer<string, string> _producer;
        private readonly string _topic;
        private readonly IMessagePublisher<PostIntegrationEvent> _messagePublisher;
        private readonly KafkaTestcontainer _kafkaTestContainers;
        public MessageHandler_Spec(CustomWebApplicationFactory<Startup> factory)
        {
            Random rnd = new Random();
            var testcontainersBuilder = new TestcontainersBuilder<KafkaTestcontainer>()
                                        .WithImage("confluentinc/cp-kafka:6.0.1")
                                        .WithCleanUp(true)
                                        .WithKafka(new KafkaTestcontainerConfiguration()
                                        {
                                            Port = 9092
                                        });
            this._kafkaTestContainers = testcontainersBuilder.Build();
            this._kafkaTestContainers.StartAsync().ConfigureAwait(false).GetAwaiter().GetResult();

            this._producer = factory.Services.CreateScope().ServiceProvider.GetService<IProducer<string, string>>();
            this._topic = factory.Services.CreateScope().ServiceProvider.GetService<IOptions<KafkaProducerConfig>>().Value.Topic;
            this._messagePublisher = factory.Services.CreateScope().ServiceProvider.GetService<IMessagePublisher<PostIntegrationEvent>>();
        }

        public void Dispose()
        {
            this._kafkaTestContainers.CleanUpAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }


        [Fact]
        public async Task Publish_Should_Send_Message_To_Message_Queue()
        {
            await Task.Delay(2000);
            var category = new Category { Id = Guid.NewGuid().ToString(), Name = "category" };

            var post = new PostAggregateDataModel
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "http://localhost",
                AuthorEmail = "abc@gmail.com",
                AuthorId = Guid.NewGuid().ToString()
            };

            var postIntegrationEvent = new PostIntegrationEvent
            {
                Head = new Domain.IntegrationEvents.Header()
                {
                    EvenType = "C"
                },
                Body = new Body()
                {
                    Id = post.Id,
                    Title = post.Title,
                    Thumbnail = post.Thumbnail,
                    Content = post.Content,
                    AuthorId = post.AuthorId,
                    AuthorEmail = post.AuthorEmail,
                    Categories = new List<CategoryEvent>() { new CategoryEvent { Id = category.Id.ToString(), Name = category.Name } }
                }
            };

            Func<Task> actual = () =>  this._messagePublisher.Publish(postIntegrationEvent);
            await actual.Should().NotThrowAsync();
        }
    }
}
