﻿using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Repository.CacheRepository;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Api.IntegrationTest.InfrastructureTest.RepositoryTest.CacheRepositoryTest
{
    public class CacheRepository_Spec : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ICacheRepository<PostAggregateDataModel> _cacheRepository;
        private readonly IDistributedCache _distributedCache;
        public CacheRepository_Spec(CustomWebApplicationFactory<Startup> factory)
        {
            this._distributedCache = factory.Services.CreateScope().ServiceProvider.GetService<IDistributedCache>();
            this._cacheRepository = factory.Services.CreateScope().ServiceProvider.GetService<ICacheRepository<PostAggregateDataModel>>();
        }

        [Fact]
        public async Task Get_ById_Should_Return_Post_Aggregate_Data_Model()
        {
            var category = new Category { Id = Guid.NewGuid().ToString(), Name = "category" };
            var post = new PostAggregateDataModel
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "http://localhost",
                AuthorEmail = "abc@gmail.com",
                AuthorId = Guid.NewGuid().ToString(),
                Categories = new List<Category>() { category }
            };

            var jsonStringValue = JsonSerializer.Serialize(post);
            var valueEncode = Encoding.UTF8.GetBytes(jsonStringValue);
            await this._distributedCache.SetAsync(post.Id.ToString(), valueEncode);

            var postValue = await this._cacheRepository.GetByKeyAsync(post.Id.ToString());

            postValue.Should().NotBeNull();
            postValue.Id.Should().Be(post.Id);
            postValue.Title.Should().Be(post.Title);
            postValue.Content.Should().Be(post.Content);
            postValue.Thumbnail.Should().Be(post.Thumbnail);
            postValue.AuthorId.Should().Be(post.AuthorId);
            postValue.AuthorEmail.Should().Be(post.AuthorEmail);

            postValue.Categories.Should().NotBeEmpty();
            postValue.Categories.Should().ContainSingle();
            postValue.Categories.Should().SatisfyRespectively(fist =>
            {
                fist.Id.Should().Be(post.Categories.FirstOrDefault().Id);
                fist.Name.Should().Be(post.Categories.FirstOrDefault().Name);
            });

        }
    }
}
