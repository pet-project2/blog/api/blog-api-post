﻿using Api.IntegrationTest.Helper;
using Elasticsearch.Net;
using FluentAssertions;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.Models;
using Infrastructure.Repository.SearchRepository;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Api.IntegrationTest.InfrastructureTest.RepositoryTest.SearchRepositoryTest
{
    public class SearchRepository_Spec : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        public SearchRepository_Spec(CustomWebApplicationFactory<Startup> factory)
        {
            this._factory = factory;
        }

        [Fact]
        public async Task GetById_Should_Return_Post_Aggregate_Data_Mode_When_Query_In_Search_Engine()
        {
            var postId = "4c870ee4-582b-40f6-86b3-166df3df31a1";
            var stringPath = "FakeData/FakeElasticsearchData/Post__Response_ById_4c870ee4-582b-40f6-86b3-166df3df31a1.json";
            
            var serviceProvider = _factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    ConfigHelper.ConfigElasticSearch(services, stringPath);
                });
            }).Services;

            var searchRepository = serviceProvider.CreateScope().ServiceProvider.GetService<ISearchRepository<PostAggregateDataModel>>();

            var actuals = await searchRepository.GetByIdAsync(postId);

            actuals.Should().NotBeNull();
        }
    }
}
