﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Api.IntegrationTest.ControllerTest
{
    public class PostControllerTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _httpClient;
        public PostControllerTest(CustomWebApplicationFactory<Startup> factory)
        {
            this._httpClient = factory.CreateDefaultClient();
        }
    }
}
