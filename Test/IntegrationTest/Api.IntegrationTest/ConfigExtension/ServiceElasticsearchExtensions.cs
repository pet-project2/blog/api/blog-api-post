﻿using Elasticsearch.Net;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.Models;
using Infrastructure.Repository.SearchRepository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Api.IntegrationTest.ConfigExtension
{
    public static class ServiceElasticsearchExtensions
    {
        public static void AddElasticsearch(this IServiceCollection services, IConfiguration configuration)
        {
            var esSetting = configuration.GetSection("Elasticsearch").Get<ElasticsearchConfig>();
            var connectionPool = new SingleNodeConnectionPool(new Uri(esSetting.Uri));
            var settings = new ConnectionSettings(connectionPool, new InMemoryConnection())
                           .DefaultIndex(esSetting.Index)
                           .EnableDebugMode()
                           .OnRequestCompleted(response =>
                           {
                               // log out the request
                               if (response.RequestBodyInBytes != null)
                               {
                                   Debug.WriteLine(
                                       $"{response.HttpMethod} {response.Uri} \n" +
                                       $"{Encoding.UTF8.GetString(response.RequestBodyInBytes)}");
                               }
                               else
                               {
                                   Debug.WriteLine($"{response.HttpMethod} {response.Uri}");
                               }

                               Debug.WriteLine("-------");
                               // log out the response
                               if (response.ResponseBodyInBytes != null)
                               {
                                   Debug.WriteLine($"Status: {response.HttpStatusCode}\n" +
                                            $"{Encoding.UTF8.GetString(response.ResponseBodyInBytes)}\n" +
                                            $"{new string('-', 30)}\n");
                               }
                               else
                               {
                                   Debug.WriteLine($"Status: {response.HttpStatusCode}\n" +
                                            $"{new string('-', 30)}\n");
                               }

                           });

            var client = new ElasticClient(settings);
            esSetting.AddPostMapping(client); // add post Mapping
            services.AddSingleton<IElasticClient>(client);
            services.AddScoped<ISearchRepository<PostAggregateDataModel>, SearchRepository>();
        }
    }
}
