﻿using Infrastructure.IntegrationAppConfig;
using Infrastructure.Models;
using Infrastructure.Repository.CacheRepository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.IntegrationTest.ConfigExtension
{
    public static class ServiceCacheExtensions
    {
        public static void AddCache(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDistributedMemoryCache(options => 
            {
                options.ExpirationScanFrequency = TimeSpan.FromSeconds(5);
            });

            var redisConfig = configuration.GetSection("Redis");
            var redisSettings = redisConfig.Get<RedisConfig>();

            // add setting 
            services.Configure<RedisConfig>(redisConfig);
            services.AddScoped(typeof(ICacheRepository<>), typeof(CacheRepository<>));
        }
    }
}
