﻿using Confluent.Kafka;
using DotNet.Testcontainers.Containers.Builders;
using DotNet.Testcontainers.Containers.Configurations.MessageBrokers;
using DotNet.Testcontainers.Containers.Modules.MessageBrokers;
using Infrastructure.IntegrationAppConfig;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.IntegrationTest.ConfigExtension
{
    public static class ServiceKafkaExtensions
    {
        public static void AddKafkaProducer<TKey, TValue>(this IServiceCollection services, IConfiguration configuration)
        {
            var kafkaConfig = configuration.GetSection("Kafka");
            var kafkaSettings = kafkaConfig.Get<KafkaProducerConfig>();
            // add setting 
            services.Configure<KafkaProducerConfig>(kafkaConfig);

            var producerConfig = new ProducerConfig
            {
                BootstrapServers = kafkaSettings.Uri
            };

            var producerBuilder = new ProducerBuilder<TKey, TValue>(producerConfig).Build();
            services.AddSingleton(producerBuilder);
        }
    }
}
