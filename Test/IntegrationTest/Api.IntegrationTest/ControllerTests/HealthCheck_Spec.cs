﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Api.IntegrationTest.ControllerTests
{
    public class HealthCheck_Spec : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _httpClient;

        public HealthCheck_Spec(CustomWebApplicationFactory<Startup> factory)
        {
            this._httpClient = factory.CreateDefaultClient();
        }

        [Fact]
        public async Task Should_HealthCheck_Return_Ok()
        {
            var response = await _httpClient.GetAsync("/health");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
