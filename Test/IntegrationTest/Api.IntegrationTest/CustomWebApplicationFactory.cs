﻿using Api.IntegrationTest.ConfigExtension;
using Domain.IntegrationEvents;
using Fury.Core.Interface;
using Infrastructure;
using Infrastructure.MessageHandler;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Api.IntegrationTest
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        private readonly InMemoryDatabaseRoot _dbRoot = new InMemoryDatabaseRoot();
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            var configuration = new ConfigurationBuilder()
                                     .SetBasePath(Directory.GetCurrentDirectory())
                                     .AddJsonFile("appsettings.json")
                                     .AddEnvironmentVariables()
                                     .Build();

            builder.UseEnvironment("Integration-Test");

            builder.ConfigureTestServices(services =>
            {
                services.AddDbContext<AppDbContext>(options =>
                {
                    options.UseInMemoryDatabase(Guid.NewGuid().ToString(), _dbRoot);
                });

                // setting inmemory elasticsearch
                services.AddCache(configuration);
                services.AddElasticsearch(configuration);
                services.AddKafkaProducer<string, string>(configuration);
                services.AddScoped<IMessagePublisher<PostIntegrationEvent>, EventMessageHandler>();
            });
        }
    }
}
