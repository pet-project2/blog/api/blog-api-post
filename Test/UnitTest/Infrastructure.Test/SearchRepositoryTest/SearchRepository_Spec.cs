﻿using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Repository.SearchRepository;
using Moq;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Test.SearchRepositoryTest
{
   public class SearchRepository_Spec
    {
        private readonly Mock<IElasticClient> _mockElasticClient;
        private readonly Mock<ISearchResponse<PostAggregateDataModel>> _mockSearchResponse;

        public SearchRepository_Spec()
        {
            this._mockElasticClient = new Mock<IElasticClient>();
            this._mockSearchResponse = new Mock<ISearchResponse<PostAggregateDataModel>>();
        }

        [Fact]
        public async Task Should_Return_Null_When_Get_By_Id_With_Null_Source_GetAsync()
        {
            this._mockElasticClient.Setup(c => c.GetAsync(It.IsAny<DocumentPath<PostAggregateDataModel>>(), It.IsAny<Func<GetDescriptor<PostAggregateDataModel>, IGetRequest>>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new GetResponse<PostAggregateDataModel>());

            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetByIdAsync("abc");

            actuals.Should().BeNull();
        }


        [Fact]
        public async Task Should_Return_Null_When_GetById_With_Throw_Exception_GetAsync()
        {
            var category = new Models.Category()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "category"
            };

            var postAggregateDataModel = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid().ToString(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<Models.Category>() { category }
            };

            this._mockElasticClient.Setup(c => c.GetAsync(It.IsAny<DocumentPath<PostAggregateDataModel>>(), It.IsAny<Func<GetDescriptor<PostAggregateDataModel>, IGetRequest>>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception());

            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetByIdAsync("abc");

            actuals.Should().BeNull();
        }

        [Theory]
        [InlineData("")]
        [InlineData((string)null)]
        public async Task Should_Return_Null_When_GetById_With_Id_Is_Null_Or_Empty(string id)
        {
            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetByIdAsync(id);

            actuals.Should().BeNull();
        }

        [Fact]
        public async Task Should_Return_Post_Aggregate_Data_Model_When_GetByIdWithAuthorIdAsync_With_Normal_Query_Result_SearchAsync()
        {
            var category = new Models.Category()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "category"
            };

            var postAggregateDataModel = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid().ToString(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<Models.Category>() { category }
            };

            var listPost = new List<PostAggregateDataModel>() { postAggregateDataModel };

            this._mockSearchResponse.Setup(r => r.Documents).Returns(listPost.AsReadOnly());

            this._mockElasticClient.Setup(x => x.SearchAsync(It.IsAny<Func<SearchDescriptor<PostAggregateDataModel>, ISearchRequest>>(), It.IsAny<CancellationToken>())).ReturnsAsync(this._mockSearchResponse.Object);

            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetByIdWithAuthorIdAsync("abc", "xyz");

            actuals.Should().NotBeNull();
            actuals.Id.Should().Be(postAggregateDataModel.Id);
            actuals.Title.Should().Be(postAggregateDataModel.Title);
            actuals.Content.Should().Be(postAggregateDataModel.Content);
            actuals.Thumbnail.Should().Be(postAggregateDataModel.Thumbnail);
            actuals.CreatedAt.Should().Be(postAggregateDataModel.CreatedAt);
            actuals.LastModifiedAt.Should().Be(postAggregateDataModel.LastModifiedAt);
            actuals.AuthorId.Should().Be(postAggregateDataModel.AuthorId);
            actuals.AuthorEmail.Should().Be(postAggregateDataModel.AuthorEmail);

            actuals.Categories.Should().ContainSingle();
            actuals.Categories.Should().SatisfyRespectively(first => {
                first.Id.Should().Be(category.Id);
                first.Name.Should().Be(category.Name);
            });
        }

        [Fact]
        public async Task Should_Return_Null_When_GetByIdWithAuthorIdAsync_With_List_Empty_Query_Result_SearchAsync()
        {
            var listPost = new List<PostAggregateDataModel>();

            this._mockSearchResponse.Setup(r => r.Documents).Returns(listPost.AsReadOnly());

            this._mockElasticClient.Setup(x => x.SearchAsync(It.IsAny<Func<SearchDescriptor<PostAggregateDataModel>, ISearchRequest>>(), It.IsAny<CancellationToken>())).ReturnsAsync(this._mockSearchResponse.Object);

            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetByIdWithAuthorIdAsync("abc", "xyz");

            actuals.Should().BeNull();
        }

        [Fact]
        public async Task Should_Return_Null_When_GetByIdWithAuthorIdAsync_With_List_Null_Query_Result_SearchAsync()
        {
            this._mockSearchResponse.Setup(r => r.Documents).Returns((List<PostAggregateDataModel>)null);

            this._mockElasticClient.Setup(x => x.SearchAsync(It.IsAny<Func<SearchDescriptor<PostAggregateDataModel>, ISearchRequest>>(), It.IsAny<CancellationToken>())).ReturnsAsync(this._mockSearchResponse.Object);

            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetByIdWithAuthorIdAsync("abc", "xyz");

            actuals.Should().BeNull();
        }

        [Fact]
        public async Task Should_Return_Null_When_GetByIdWithAuthorIdAsync_With_Throw_Exception_SearchAsync()
        {
            this._mockSearchResponse.Setup(r => r.Documents).Returns((List<PostAggregateDataModel>)null);

            this._mockElasticClient.Setup(x => x.SearchAsync(It.IsAny<Func<SearchDescriptor<PostAggregateDataModel>, ISearchRequest>>(), It.IsAny<CancellationToken>())).ThrowsAsync(new Exception());

            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetByIdWithAuthorIdAsync("abc", "xyz");

            actuals.Should().BeNull();
        }

        [Theory]
        [InlineData("", "abc")]
        [InlineData("xyz", "")]
        [InlineData("", "")]
        [InlineData((string)null, null)]
        public async Task Should_Return_Null_When_GetByIdWithAuthorIdAsync_With_Id_And_AuthorId_Is_Null_Or_Empty_SearchAsync(string id, string authorId)
        {
            var searchRepository = new SearchRepository(this._mockElasticClient.Object);
            var actuals = await searchRepository.GetByIdWithAuthorIdAsync(id, authorId);
            actuals.Should().BeNull();
        }

        [Fact]
        public async Task Should_Return_List_Post_Aggregate_Data_Model_With_AuthorId_When_GetByAuthorIdAsync_Which_SearchAsync_Successfull()
        {
            var category = new Models.Category()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "category"
            };

            var postAggregateDataModel = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid().ToString(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<Models.Category>() { category }
            };

            var listPost = new List<PostAggregateDataModel>() { postAggregateDataModel };

            this._mockSearchResponse.Setup(r => r.Documents).Returns(listPost.AsReadOnly());

            this._mockElasticClient.Setup(x => x.SearchAsync(It.IsAny<Func<SearchDescriptor<PostAggregateDataModel>, ISearchRequest>>(), It.IsAny<CancellationToken>())).ReturnsAsync(this._mockSearchResponse.Object);


            var searchRepository = new SearchRepository(this._mockElasticClient.Object);

            var actuals = await searchRepository.GetAllByAuthorIdAsync("abc");

            actuals.Should().HaveCount(1);
            actuals.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(postAggregateDataModel.Id);
                first.Title.Should().Be(postAggregateDataModel.Title);
                first.Content.Should().Be(postAggregateDataModel.Content);
                first.Thumbnail.Should().Be(postAggregateDataModel.Thumbnail);
                first.CreatedAt.Should().Be(postAggregateDataModel.CreatedAt);
                first.LastModifiedAt.Should().Be(postAggregateDataModel.LastModifiedAt);
                first.AuthorId.Should().Be(postAggregateDataModel.AuthorId);
                first.AuthorEmail.Should().Be(postAggregateDataModel.AuthorEmail);

            });

        }
    }
}
