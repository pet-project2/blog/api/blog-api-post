﻿using FluentAssertions;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.Models;
using Infrastructure.Repository.CacheRepository;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Test.CacheRepositoryTest
{
    public class CacheRepository_Spec
    {
        private readonly Mock<IDistributedCache> _mockDistributedCache;
        private readonly Mock<ILogger<CacheRepository<PostAggregateDataModel>>> _mockLogger;
        private readonly IOptions<RedisConfig> _options;
        public CacheRepository_Spec()
        {
            this._mockDistributedCache = new Mock<IDistributedCache>();
            this._mockLogger = new Mock<ILogger<CacheRepository<PostAggregateDataModel>>>();
            this._options = Options.Create(new RedisConfig()
            {
                AbsoluteExpiredTime = "3600",
                SlidingExpiredTime = "300"
            });
        }

        [Fact]
        public async Task Should_Return_Object_When_Redis_Have_Value_By_Key()
        {
            var post = new PostAggregateDataModel
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "http://localhost",
                AuthorEmail = "abc@gmail.com",
                AuthorId = Guid.NewGuid().ToString(),
                Categories = new List<Category>() { new Category { Id = Guid.NewGuid().ToString(), Name = "category" } }
            };

            var byteValue = JsonSerializer.SerializeToUtf8Bytes(post);

            this._mockDistributedCache.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(byteValue);

            var redisRepo = new CacheRepository<PostAggregateDataModel>(this._mockDistributedCache.Object, this._options, this._mockLogger.Object);

            var postValue = await redisRepo.GetByKeyAsync("abc");

            postValue.Should().NotBeNull();
            postValue.Id.Should().Be(post.Id);
            postValue.Title.Should().Be(post.Title);
            postValue.Content.Should().Be(post.Content);
            postValue.Thumbnail.Should().Be(post.Thumbnail);
            postValue.AuthorId.Should().Be(post.AuthorId);
            postValue.AuthorEmail.Should().Be(post.AuthorEmail);

            postValue.Categories.Should().NotBeEmpty();
            postValue.Categories.Should().ContainSingle();
            postValue.Categories.Should().SatisfyRespectively(fist =>
            {
                fist.Id.Should().Be(post.Categories.FirstOrDefault().Id);
                fist.Name.Should().Be(post.Categories.FirstOrDefault().Name);
            });
        }


        [Fact]
        public async Task Should_Return_Null_When_Redis_Does_Not_Have_Value_By_Key()
        {
            byte[] byteArray = null;

            this._mockDistributedCache.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(byteArray);

            var redisRepo = new CacheRepository<PostAggregateDataModel>(this._mockDistributedCache.Object, this._options, this._mockLogger.Object);

            var postValue = await redisRepo.GetByKeyAsync("abc");
            postValue.Should().BeNull();
        }


        [Fact]
        public async Task Should_Return_Null_When_GetByKeyAsync_Which_Redis_Throw_Exception_GetAsync()
        {
            this._mockDistributedCache.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                                      .ThrowsAsync(new Exception());

            var redisRepo = new CacheRepository<PostAggregateDataModel>(this._mockDistributedCache.Object, this._options, this._mockLogger.Object);

            var postValue = await redisRepo.GetByKeyAsync("abc");

            postValue.Should().BeNull();
        }

        [Fact]
        public async Task Should_Return_List_Object_When_Redis_Have_Value_By_Key()
        {
            var post = new PostAggregateDataModel
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "http://localhost",
                AuthorEmail = "abc@gmail.com",
                AuthorId = Guid.NewGuid().ToString(),
                Categories = new List<Category>() { new Category { Id = Guid.NewGuid().ToString(), Name = "category" } }
            };
            var listPost = new List<PostAggregateDataModel>()
            {
                 post
            };

            var byteValue = JsonSerializer.SerializeToUtf8Bytes(listPost);

            this._mockDistributedCache.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(byteValue);

            var redisRepo = new CacheRepository<PostAggregateDataModel>(this._mockDistributedCache.Object, this._options, this._mockLogger.Object);

            var postValue = await redisRepo.GetListByKeyAsync("abc");

            postValue.Should().NotBeNull();
            postValue.Should().ContainSingle();
            postValue.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(post.Id);
                first.Title.Should().Be(post.Title);
                first.Content.Should().Be(post.Content);
                first.Thumbnail.Should().Be(post.Thumbnail);
                first.AuthorId.Should().Be(post.AuthorId);
                first.AuthorEmail.Should().Be(post.AuthorEmail);

                first.Categories.Should().NotBeEmpty();
                first.Categories.Should().ContainSingle();
                first.Categories.Should().SatisfyRespectively(fistCate =>
                {

                    fistCate.Id = post.Categories.FirstOrDefault().Id;
                    fistCate.Name = post.Categories.FirstOrDefault().Name;
                });
            });
        }

        [Fact]
        public async Task Should_Return_Null_When_GetListByKeyAsync_Which_Redis_Does_Not_Have_Value_By_Key()
        {
            this._mockDistributedCache.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync((byte[])null);

            var redisRepo = new CacheRepository<PostAggregateDataModel>(this._mockDistributedCache.Object, this._options, this._mockLogger.Object);

            var postValue = await redisRepo.GetListByKeyAsync("abc");

            postValue.Should().BeNull();
        }


        [Fact]
        public async Task Should_Return_Null_When_GetListByKeyAsync_Which_Redis_Throw_Exception_GetAsync()
        {
            this._mockDistributedCache.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                                      .ThrowsAsync(new Exception());

            var redisRepo = new CacheRepository<PostAggregateDataModel>(this._mockDistributedCache.Object, this._options, this._mockLogger.Object);

            var postValue = await redisRepo.GetListByKeyAsync("abc");

            postValue.Should().BeNull();
        }
    }
}
