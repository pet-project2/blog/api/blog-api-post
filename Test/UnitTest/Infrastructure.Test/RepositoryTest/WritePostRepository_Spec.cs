﻿using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using Infrastructure.Repository;
using Infrastructure.Repository.EfRepository;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Test.RepositoryTest
{
    public class WritePostRepository_Spec
    {
        private readonly Mock<IEfRepository<PostEf, PostAggregateDataModel>> _mockEfRepository;
        public WritePostRepository_Spec()
        {
            this._mockEfRepository = new Mock<IEfRepository<PostEf, PostAggregateDataModel>>();
        }

        [Fact]
        public void Should_Add_Post_Return_No_Exception()
        {
            this._mockEfRepository.Setup(x => x.AddNoReturnAsync(It.IsAny<PostEf>(), It.IsAny<CancellationToken>()))
                                  .Returns(Task.FromResult<object>(null));

            var writePostRepository = new WritePostRepository(this._mockEfRepository.Object);

            Func<Task> actual = async () => await writePostRepository.AddAsync(new PostAggregateDataModel());

            actual.Should().NotThrow<Exception>();
        }
    }
}
