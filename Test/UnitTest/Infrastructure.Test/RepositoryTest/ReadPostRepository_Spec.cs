﻿using Domain;
using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using Infrastructure.Repository;
using Infrastructure.Repository.CacheRepository;
using Infrastructure.Repository.EfRepository;
using Infrastructure.Repository.SearchRepository;
using Infrastructure.Specifications.PostSpec;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Test.RepositoryTest
{
    public class ReadPostRepository_Spec
    {
        private readonly Mock<ICacheRepository<PostAggregateDataModel>> _mockCacheRepository;
        private readonly Mock<ISearchRepository<PostAggregateDataModel>> _mockSearchRepository;
        private readonly Mock<IEfRepository<PostEf, PostAggregateDataModel>> _mockEfRepository;
        public ReadPostRepository_Spec()
        {
            _mockCacheRepository = new Mock<ICacheRepository<PostAggregateDataModel>>();
            _mockSearchRepository = new Mock<ISearchRepository<PostAggregateDataModel>>();
            _mockEfRepository = new Mock<IEfRepository<PostEf, PostAggregateDataModel>>();
        }

        [Fact]
        public async Task Should_Get_By_Id_With_Author_Id_Return_Post_Aggreate_Data_Model_From_Cache()
        {
            var category = new Models.Category() { 
                Id = Guid.NewGuid().ToString(),
                Name = "category"
            };

            var postAggregateDataModel = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid().ToString(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<Models.Category>() { category }
            };

            _mockCacheRepository.Setup(x => x.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync(postAggregateDataModel);

            var readPostRepository = new ReadPostRepository(_mockCacheRepository.Object, _mockSearchRepository.Object, _mockEfRepository.Object);

            var actuals = await readPostRepository.GetByIdWithAuthorIdAsync("abc", "xyz");

            actuals.Should().NotBeNull();
            actuals.Id.Should().Be(postAggregateDataModel.Id);
            actuals.Title.Should().Be(postAggregateDataModel.Title);
            actuals.Content.Should().Be(postAggregateDataModel.Content);
            actuals.Thumbnail.Should().Be(postAggregateDataModel.Thumbnail);
            actuals.CreatedAt.Should().Be(postAggregateDataModel.CreatedAt);
            actuals.LastModifiedAt.Should().Be(postAggregateDataModel.LastModifiedAt);
            actuals.AuthorId.Should().Be(postAggregateDataModel.AuthorId);
            actuals.AuthorEmail.Should().Be(postAggregateDataModel.AuthorEmail);

            actuals.Categories.Should().ContainSingle();
            actuals.Categories.Should().SatisfyRespectively(first => {
                first.Id.Should().Be(category.Id);
                first.Name.Should().Be(category.Name);
            });
        }

        [Fact]
        public async Task Should_Get_By_Id_With_Author_Id_Return_Post_Aggreate_Data_Model_From_Search_Engine()
        {
            var category = new Models.Category()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "category"
            };

            var postAggregateDataModel = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid().ToString(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<Models.Category>() { category }
            };

            _mockCacheRepository.Setup(x => x.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync((PostAggregateDataModel)null);
            _mockSearchRepository.Setup(x => x.GetByIdWithAuthorIdAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(postAggregateDataModel);

            var readPostRepository = new ReadPostRepository(_mockCacheRepository.Object, _mockSearchRepository.Object, _mockEfRepository.Object);

            var actuals = await readPostRepository.GetByIdWithAuthorIdAsync("abc", "xyz");

            actuals.Should().NotBeNull();
            actuals.Id.Should().Be(postAggregateDataModel.Id);
            actuals.Title.Should().Be(postAggregateDataModel.Title);
            actuals.Content.Should().Be(postAggregateDataModel.Content);
            actuals.Thumbnail.Should().Be(postAggregateDataModel.Thumbnail);
            actuals.CreatedAt.Should().Be(postAggregateDataModel.CreatedAt);
            actuals.LastModifiedAt.Should().Be(postAggregateDataModel.LastModifiedAt);
            actuals.AuthorId.Should().Be(postAggregateDataModel.AuthorId);
            actuals.AuthorEmail.Should().Be(postAggregateDataModel.AuthorEmail);

            actuals.Categories.Should().ContainSingle();
            actuals.Categories.Should().SatisfyRespectively(first => {
                first.Id.Should().Be(category.Id);
                first.Name.Should().Be(category.Name);
            });
        }

        [Fact]
        public async Task Should_Get_By_Id_With_Author_Id_Return_Post_Aggreate_Data_Model_From_Ef()
        {
            var category = new CategoryEf()
            {
                Id = Guid.NewGuid(),
                CateId = Guid.NewGuid(),
                Name = "category"
            };

            var postEfModel = new PostEf()
            {
                Id = Guid.NewGuid(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreateAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<CategoryEf>() { category }
            };

            _mockCacheRepository.Setup(x => x.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync((PostAggregateDataModel)null);
            _mockSearchRepository.Setup(x => x.GetByIdWithAuthorIdAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync((PostAggregateDataModel)null);
            _mockEfRepository.Setup(x => x.GetBySpecAsync(It.IsAny<PostByIdAndAuthorIdSpec>(), It.IsAny<CancellationToken>())).ReturnsAsync(postEfModel);


            var readPostRepository = new ReadPostRepository(_mockCacheRepository.Object, _mockSearchRepository.Object, _mockEfRepository.Object);

            var actuals = await readPostRepository.GetByIdWithAuthorIdAsync(Guid.NewGuid().ToString(), Guid.NewGuid().ToString());

            actuals.Should().NotBeNull();
            actuals.Id.Should().Be(postEfModel.Id.ToString());
            actuals.Title.Should().Be(postEfModel.Title);
            actuals.Content.Should().Be(postEfModel.Content);
            actuals.Thumbnail.Should().Be(postEfModel.Thumbnail);
            actuals.CreatedAt.Should().Be(postEfModel.CreateAt);
            actuals.LastModifiedAt.Should().Be(postEfModel.LastModifiedAt);
            actuals.AuthorId.Should().Be(postEfModel.AuthorId.ToString());
            actuals.AuthorEmail.Should().Be(postEfModel.AuthorEmail);

            actuals.Categories.Should().ContainSingle();
            actuals.Categories.Should().SatisfyRespectively(first => {
                first.Id.Should().Be(category.CateId.ToString());
                first.Name.Should().Be(category.Name);
            });
        }

        [Fact]
        public async Task Should_Get_List_By_Author_Return_List_Post_Aggreate_Data_Model_From_Cache()
        {
            var category = new Models.Category()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "category"
            };

            var postAggregateDataModel = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid().ToString(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<Models.Category>() { category }
            };

            var listPost = new List<PostAggregateDataModel>() { postAggregateDataModel };

            _mockCacheRepository.Setup(x => x.GetListByKeyAsync(It.IsAny<string>())).ReturnsAsync(listPost);

            var readPostRepository = new ReadPostRepository(_mockCacheRepository.Object, _mockSearchRepository.Object, _mockEfRepository.Object);

            var actuals = await readPostRepository.GetAllByAuthorIdAsync("abc");

            actuals.Should().ContainSingle();
            actuals.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(postAggregateDataModel.Id);
                first.Title.Should().Be(postAggregateDataModel.Title);
                first.Thumbnail.Should().Be(postAggregateDataModel.Thumbnail);
                first.Content.Should().Be(postAggregateDataModel.Content);
                first.CreatedAt.Should().Be(postAggregateDataModel.CreatedAt);
                first.LastModifiedAt.Should().Be(postAggregateDataModel.LastModifiedAt);
                first.AuthorId.Should().Be(postAggregateDataModel.AuthorId);
                first.AuthorEmail.Should().Be(postAggregateDataModel.AuthorEmail);

                first.Categories.Should().ContainSingle();
                first.Categories.Should().SatisfyRespectively(cate =>
                {
                    cate.Id.Should().Be(category.Id);
                    cate.Name.Should().Be(category.Name);
                });

            });
        }

        [Fact]
        public async Task Should_Get_List_By_Author_Return_List_Post_Aggreate_Data_Model_From_Search_Engine()
        {
            var category = new Models.Category()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "category"
            };

            var postAggregateDataModel = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid().ToString(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<Models.Category>() { category }
            };

            var listPost = new List<PostAggregateDataModel>() { postAggregateDataModel };

            _mockCacheRepository.Setup(x => x.GetListByKeyAsync(It.IsAny<string>())).ReturnsAsync((IEnumerable<PostAggregateDataModel>)null);
            _mockSearchRepository.Setup(x => x.GetAllByAuthorIdAsync(It.IsAny<string>())).ReturnsAsync(listPost);

            var readPostRepository = new ReadPostRepository(_mockCacheRepository.Object, _mockSearchRepository.Object, _mockEfRepository.Object);

            var actuals = await readPostRepository.GetAllByAuthorIdAsync("abc");

            actuals.Should().ContainSingle();
            actuals.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(postAggregateDataModel.Id);
                first.Title.Should().Be(postAggregateDataModel.Title);
                first.Thumbnail.Should().Be(postAggregateDataModel.Thumbnail);
                first.Content.Should().Be(postAggregateDataModel.Content);
                first.CreatedAt.Should().Be(postAggregateDataModel.CreatedAt);
                first.LastModifiedAt.Should().Be(postAggregateDataModel.LastModifiedAt);
                first.AuthorId.Should().Be(postAggregateDataModel.AuthorId);
                first.AuthorEmail.Should().Be(postAggregateDataModel.AuthorEmail);

                first.Categories.Should().ContainSingle();
                first.Categories.Should().SatisfyRespectively(cate =>
                {
                    cate.Id.Should().Be(category.Id);
                    cate.Name.Should().Be(category.Name);
                });

            });

        }

        [Fact]
        public async Task Should_Get_List_By_Author_Return_List_Post_Aggreate_Data_Model_From_Search_Ef()
        {
            var category = new CategoryEf()
            {
                Id = Guid.NewGuid(),
                CateId = Guid.NewGuid(),
                Name = "category"
            };

            var postEfModel = new PostEf()
            {
                Id = Guid.NewGuid(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreateAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<CategoryEf>() { category }
            };

            var listPost = new List<PostEf>() { postEfModel };

            _mockCacheRepository.Setup(x => x.GetListByKeyAsync(It.IsAny<string>())).ReturnsAsync((IEnumerable<PostAggregateDataModel>)null);
            _mockSearchRepository.Setup(x => x.GetAllByAuthorIdAsync(It.IsAny<string>())).ReturnsAsync((IEnumerable<PostAggregateDataModel>)null);


            _mockEfRepository.Setup(x => x.ListAsync(It.IsAny<PostsByAuthorIdSpec>(), It.IsAny<CancellationToken>())).ReturnsAsync(listPost);

            var readPostRepository = new ReadPostRepository(_mockCacheRepository.Object, _mockSearchRepository.Object, _mockEfRepository.Object);

            var actuals = await readPostRepository.GetAllByAuthorIdAsync(Guid.NewGuid().ToString());

            actuals.Should().ContainSingle();
            actuals.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(postEfModel.Id.ToString());
                first.Title.Should().Be(postEfModel.Title);
                first.Thumbnail.Should().Be(postEfModel.Thumbnail);
                first.Content.Should().Be(postEfModel.Content);
                first.CreatedAt.Should().Be(postEfModel.CreateAt);
                first.LastModifiedAt.Should().Be(postEfModel.LastModifiedAt);
                first.AuthorId.Should().Be(postEfModel.AuthorId.ToString());
                first.AuthorEmail.Should().Be(postEfModel.AuthorEmail);

                first.Categories.Should().ContainSingle();
                first.Categories.Should().SatisfyRespectively(cate =>
                {
                    cate.Id.Should().Be(category.CateId.ToString());
                    cate.Name.Should().Be(category.Name);
                });

            });
        }

    }
}
