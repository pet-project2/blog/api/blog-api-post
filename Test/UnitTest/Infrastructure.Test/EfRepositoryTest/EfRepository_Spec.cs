﻿using FluentAssertions;
using Infrastructure.Models.EfModels;
using Infrastructure.Repository.EfRepository;
using Infrastructure.Specifications.PostSpec;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Test.EfRepositoryTest
{
    public class EfRepository_Spec : IDisposable
    {
        private readonly AppDbContext _appDbContext;
        private readonly Mock<IMediator> _mockMediator;
        private readonly Guid _authorId;
        public EfRepository_Spec()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                            .UseInMemoryDatabase(Guid.NewGuid().ToString())
                            .Options;

            this._mockMediator = new Mock<IMediator>();

            this._mockMediator.Setup(x => x.Publish(It.IsAny<INotification>(), It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            this._appDbContext = new AppDbContext(options, this._mockMediator.Object);

            var category = new CategoryEf()
            {
                Id = Guid.NewGuid(),
                CateId = Guid.NewGuid(),
                Name = "category"
            };

            var postEf = new PostEf()
            {
                Id = Guid.NewGuid(),
                Title = "title",
                Thumbnail = "thumbnail",
                Content = "content",
                CreateAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = Guid.NewGuid(),
                AuthorEmail = "abc@gmai.com",
                Categories = new List<CategoryEf>() { category },
            };

            this._authorId = postEf.AuthorId;
            this._appDbContext.Posts.AddRange(postEf);
            this._appDbContext.SaveChanges();
        }

        public void Dispose()
        {
            this._appDbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task Should_Add_Post_With_No_Throw_Exception_When_AddNoReturnAsync_Which_DbContext_Add_Successfull()
        {
            var category = new CategoryEf()
            {
                Id = Guid.NewGuid(),
                CateId = Guid.NewGuid(),
                Name = "post-category"
            };

            var domainEvents = new List<INotification>()
            {
                new FakeAddDomainEvent()
            };

            var postEfModel = new PostEf()
            {
                Id = Guid.NewGuid(),
                Title = "post-title",
                Thumbnail = "post-thumbnail",
                Content = "post-content",
                CreateAt = DateTime.Now,
                LastModifiedAt = DateTime.Now,
                AuthorId = this._authorId,
                AuthorEmail = "post-abc@gmai.com",
                Categories = new List<CategoryEf>() { category },
                DomainEvents = domainEvents.AsReadOnly()
            };

            var efRepository = new EfRepository(this._appDbContext);
            await efRepository.AddNoReturnAsync(postEfModel);

            var spec = new GetAllPostSpec(this._authorId);
            var actuals = await efRepository.ListAsync(spec);
            var postSaved = actuals.FirstOrDefault(x => x.Id == postEfModel.Id);

            actuals.Should().NotBeEmpty();
            actuals.Should().HaveCount(2);

            postSaved.Id.Should().Be(postEfModel.Id);
            postSaved.Title.Should().Be(postEfModel.Title);
            postSaved.Content.Should().Be(postEfModel.Content);
            postSaved.Thumbnail.Should().Be(postEfModel.Thumbnail);
            postSaved.CreateAt.Should().Be(postEfModel.CreateAt);
            postSaved.LastModifiedAt.Should().Be(postEfModel.LastModifiedAt);
            postSaved.AuthorId.Should().Be(postEfModel.AuthorId);
            postSaved.AuthorEmail.Should().Be(postEfModel.AuthorEmail);

            postSaved.Categories.Should().ContainSingle();
            postSaved.Categories.Should().SatisfyRespectively(first => {
                first.CateId.Should().Be(category.CateId);
                first.Name.Should().Be(category.Name);
            });

            _mockMediator.Verify(x => x.Publish(It.IsAny<INotification>(), It.IsAny<CancellationToken>()), Times.Exactly(1));
        }

        [Fact]
        public async Task Should_Updated_Post_With_No_Throw_Exception_When_UpdateWithNewEntityAsync_Which_DbContext_Updated_Successfull()
        {
            var newCategory = new CategoryEf()
            {
                Id = Guid.NewGuid(),
                CateId = Guid.NewGuid(),
                Name = "post-category"
            };

            var efRepository = new EfRepository(this._appDbContext);
            var spec = new GetAllPostSpec(this._authorId);
            var listPost = await efRepository.ListAsync(spec);
            var oldPost = listPost.FirstOrDefault();

            var domainEvents = new List<INotification>()
            {
                new FakeUpdatedDomainEvent()
            };

            var newPost = new PostEf()
            {
                Id = oldPost.Id,
                Title = "post-title",
                Thumbnail = "post-thumbnail",
                Content = "post-content",
                CreateAt = oldPost.CreateAt,
                LastModifiedAt = DateTime.Now,
                AuthorId = oldPost.AuthorId,
                AuthorEmail = oldPost.AuthorEmail,
                Categories = new List<CategoryEf>() { newCategory },
                DomainEvents = domainEvents.AsReadOnly()
            };

            await efRepository.UpdateWithNewEntityAsync(oldPost, newPost);

            var actualsPost = await efRepository.ListAsync(spec);
            var actualsCategory = this._appDbContext.Categories.ToList();

            var postActuals = actualsPost.FirstOrDefault();

            actualsPost.Should().ContainSingle();
            actualsCategory.Should().ContainSingle();

            postActuals.Id.Should().Be(newPost.Id);
            postActuals.Title.Should().Be(newPost.Title);
            postActuals.Content.Should().Be(newPost.Content);
            postActuals.Thumbnail.Should().Be(newPost.Thumbnail);
            postActuals.CreateAt.Should().Be(newPost.CreateAt);
            postActuals.LastModifiedAt.Should().Be(newPost.LastModifiedAt);
            postActuals.AuthorId.Should().Be(newPost.AuthorId);
            postActuals.AuthorEmail.Should().Be(newPost.AuthorEmail);

            postActuals.Categories.Should().ContainSingle();
            postActuals.Categories.Should().SatisfyRespectively(first => {
                first.CateId.Should().Be(newCategory.CateId);
                first.Name.Should().Be(newCategory.Name);
            });


            actualsCategory.Should().SatisfyRespectively(first =>
            {
                first.CateId.Should().Be(newCategory.CateId);
                first.Name.Should().Be(newCategory.Name);
            });

            _mockMediator.Verify(x => x.Publish(It.IsAny<INotification>(), It.IsAny<CancellationToken>()), Times.Exactly(1));

        }


        [Fact]
        public async Task Should_Throw_Exception_With_Null_OldPost_When_UpdateWithNewEntityAsync()
        {
            var efRepository = new EfRepository(this._appDbContext);

            Func<Task> actuals = () => efRepository.UpdateWithNewEntityAsync(null, new PostEf());

            await actuals.Should().ThrowAsync<DbUpdateException>().WithMessage("Can not update when couldn't found old post");
        }
    }
}
