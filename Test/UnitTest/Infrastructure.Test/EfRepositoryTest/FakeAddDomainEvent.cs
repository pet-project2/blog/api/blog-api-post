﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Test.EfRepositoryTest
{
    public class FakeAddDomainEvent : INotification
    {
        public Guid Id { get; set; } = Guid.NewGuid();
    }
}
