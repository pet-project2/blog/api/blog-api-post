﻿using FluentAssertions;
using Infrastructure.Mapper;
using Infrastructure.Models.EfModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Test.MapperTest
{
    public class CategoryMapper_Spec
    {
        [Fact]
        public void Should_Mapping_From_Model_Category_To_Domain_Category()
        {
            var modelCategory = new Models.Category
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".NET"
            };

            var domainCategory = InfrastructureMapping.Mapper.Map<Domain.Category>(modelCategory);
            domainCategory.Id.Should().Be(modelCategory.Id);
            domainCategory.Name.Should().Be(modelCategory.Name);
        }


        [Fact]
        public void Should_Mapping_From_Domain_Category_To_Model_Category()
        {
            var domainCategory = new Domain.Category(Guid.NewGuid(), ".NET");

            var modelCategory = InfrastructureMapping.Mapper.Map<Models.Category>(domainCategory);
            modelCategory.Id.Should().Be(domainCategory.Id.ToString());
            modelCategory.Name.Should().Be(domainCategory.Name);
        }

        [Fact]
        public void Should_Mapping_From_Model_Category_To_Category_Ef()
        {
            var modelCategory = new Models.Category
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".NET"
            };

            var categoryEf = InfrastructureMapping.Mapper.Map<CategoryEf>(modelCategory);
            categoryEf.CateId.Should().Be(modelCategory.Id.ToString());
            categoryEf.Name.Should().Be(modelCategory.Name);
        }

        [Fact]
        public void Should_Mapping_From_Category_Ef_To_Model_Category()
        {
            var categoryEf = new CategoryEf()
            {
                Id = Guid.NewGuid(),
                CateId = Guid.NewGuid(),
                Name = ".NET"
            };

            var modelCategory = InfrastructureMapping.Mapper.Map<Models.Category>(categoryEf);
            modelCategory.Id.Should().Be(categoryEf.CateId.ToString());
            modelCategory.Name.Should().Be(categoryEf.Name);
        }
    }
}
