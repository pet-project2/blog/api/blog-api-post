﻿using Domain;
using Domain.Events;
using FluentAssertions;
using Infrastructure.Mapper;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using System;
using System.Collections.Generic;
using Xunit;

namespace Infrastructure.Test.MapperTest
{
    public class PostMapper_Spec
    {
        [Fact]
        public void Should_Mapping_From_Post_Aggregate_Data_Model_To_Post_Aggregate_Domain()
        {
            var category = new Models.Category { Id = Guid.NewGuid().ToString(), Name = "category" };

            var postAggregateData = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "http://localhost",
                AuthorEmail = "abc@gmail.com",
                AuthorId = Guid.NewGuid().ToString(),
                CreatedAt = DateTime.Now.ToUniversalTime(),
                LastModifiedAt = DateTime.Now.ToUniversalTime(),
                Categories = new List<Models.Category>() { category }
            };


            var postAggregate = InfrastructureMapping.Mapper.Map<Domain.PostAggregate>(postAggregateData);
            postAggregate.Should().NotBeNull();
            postAggregate.Id.Should().Be(postAggregateData.Id);
            postAggregate.Title.Should().Be(PostTitle.FromString(postAggregateData.Title));
            postAggregate.Content.Should().Be(PostContent.FromContent(postAggregateData.Content));
            postAggregate.Thumbnail.Should().Be(PostThumnail.FromPath(postAggregateData.Thumbnail));
            postAggregate.Author.Id.Should().Be(postAggregateData.AuthorId);
            postAggregate.Author.Email.Should().Be(postAggregateData.AuthorEmail);
            postAggregate.CreatedAt.Should().Be(postAggregateData.CreatedAt);
            postAggregate.LastModifiedAt.Should().Be(postAggregateData.LastModifiedAt);

            postAggregate.Categories.Should().NotBeEmpty();
            postAggregate.Categories.Should().ContainSingle();
            postAggregate.Categories.Should().SatisfyRespectively(first =>
            {

                first.Id.Should().Be(category.Id);
                first.Name.Should().Be(category.Name);
            });
        }


        [Fact]
        public void Should_Mapping_From_Post_Aggregate_Domain_To_Post_Aggregate_Data_Model()
        {
            var category = new Domain.Category(Guid.NewGuid(), "category");

            var postDomain = new PostAggregate(
                                Guid.NewGuid(),
                                "title",
                                "image",
                                "content",
                                new List<Domain.Category>() { category },
                                new Author(Guid.NewGuid(), "email")
                             );

            var postAggregateData = InfrastructureMapping.Mapper.Map<PostAggregateDataModel>(postDomain);
            postAggregateData.Should().NotBeNull();
            postAggregateData.Id.Should().Be(postDomain.Id.ToString());
            postAggregateData.Title.Should().Be(postDomain.Title.ToString());
            postAggregateData.Content.Should().Be(postDomain.Content.ToString());
            postAggregateData.Thumbnail.Should().Be(postDomain.Thumbnail.ToString());
            postAggregateData.CreatedAt.Should().Be(postDomain.CreatedAt);
            postAggregateData.LastModifiedAt.Should().Be(postDomain.LastModifiedAt);

            postAggregateData.Categories.Should().NotBeEmpty();
            postAggregateData.Categories.Should().ContainSingle();
            postAggregateData.Categories.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(category.Id.ToString());
                first.Name.Should().Be(category.Name);
            });
        }

        [Fact]
        public void Should_Mapping_From_Post_Aggregate_Domain_To_Post_Ef()
        {
            var category = new Domain.Category(Guid.NewGuid(), "category");

            var postDomain = new PostAggregate(
                                Guid.NewGuid(),
                                "title",
                                "image",
                                "content",
                                new List<Domain.Category>() { category },
                                new Author(Guid.NewGuid(), "email")
                             );

            postDomain.AddPostCreatedDomainEvent();

            var postAggregateData = InfrastructureMapping.Mapper.Map<PostAggregateDataModel>(postDomain);
            var postEf = InfrastructureMapping.Mapper.Map<PostEf>(postAggregateData);

            postEf.Should().NotBeNull();
            postEf.Id.Should().Be(postAggregateData.Id);
            postEf.Title.Should().Be(postAggregateData.Title);
            postEf.Thumbnail.Should().Be(postAggregateData.Thumbnail);
            postEf.Content.Should().Be(postAggregateData.Content);
            postEf.AuthorId.Should().Be(postAggregateData.AuthorId);
            postEf.AuthorEmail.Should().Be(postAggregateData.AuthorEmail);
            postEf.CreateAt.Should().Be(postAggregateData.CreatedAt);
            postEf.LastModifiedAt.Should().Be(postAggregateData.LastModifiedAt);

            postEf.Categories.Should().NotBeEmpty();
            postEf.Categories.Should().ContainSingle();
            postEf.Categories.Should().SatisfyRespectively(first =>
            {

                first.CateId.Should().Be(category.Id.ToString());
                first.Name.Should().Be(category.Name);
            });

            postEf.DomainEvents.Should().NotBeEmpty();
            postEf.DomainEvents.Should().ContainSingle();
            postEf.DomainEvents.Should().SatisfyRespectively(first => {

                first.Should().BeOfType(typeof(PostAddedEvent));
            });
        }


        [Fact]
        public void Should_Mapping_From_Post_Ef_To_Post_Aggregate_Data_Model()
        {
            var category = new CategoryEf()
            {
                Id = Guid.NewGuid(),
                CateId = Guid.NewGuid(),
                Name = ".NEt"
            };

            var postEf = new PostEf()
            {
                Id = Guid.NewGuid(),
                Title = "title",
                Content = "content",
                Thumbnail = "http://localhost",
                AuthorEmail = "abc@gmail.com",
                AuthorId = Guid.NewGuid(),
                CreateAt = DateTime.Now.ToUniversalTime(),
                LastModifiedAt = DateTime.Now.ToUniversalTime(),
                Categories = new List<CategoryEf>() { category }
            };

            var postAggregateData = InfrastructureMapping.Mapper.Map<PostAggregateDataModel>(postEf);

            postAggregateData.Should().NotBeNull();
            postAggregateData.Id.Should().Be(postEf.Id.ToString());
            postAggregateData.Title.Should().Be(postEf.Title);
            postAggregateData.Content.Should().Be(postEf.Content);
            postAggregateData.Thumbnail.Should().Be(postEf.Thumbnail);
            postAggregateData.CreatedAt.Should().Be(postEf.CreateAt);
            postAggregateData.LastModifiedAt.Should().Be(postEf.LastModifiedAt);

            postAggregateData.Categories.Should().NotBeEmpty();
            postAggregateData.Categories.Should().ContainSingle();
            postAggregateData.Categories.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(category.CateId.ToString());
                first.Name.Should().Be(category.Name);
            });
        }
    }
}
