﻿using Application.Command.Update;
using Application.Dto;
using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Repository;
using MediatR;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.CommandTest
{
    public class UpdatePostCommandHandler_Spec
    {
        private readonly Mock<IWritePostRepository<PostAggregateDataModel>> _mockWritePostRepository;
        private readonly Mock<IReadPostRepository<PostAggregateDataModel>> _mockReadPostRepository;
        public UpdatePostCommandHandler_Spec()
        {
            this._mockWritePostRepository = new Mock<IWritePostRepository<PostAggregateDataModel>>();
            this._mockReadPostRepository = new Mock<IReadPostRepository<PostAggregateDataModel>>();
        }

        [Fact]
        public async Task UpdatePostCommandHandler_Should_Handle_UpdatePostCommand_Request()
        {
            this._mockWritePostRepository
                .Setup(x => x.UpdateAsync(It.IsAny<PostAggregateDataModel>(), It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            var postId = Guid.NewGuid();
            var authorId = Guid.NewGuid();

            var postAggregateModel = new PostAggregateDataModel()
            {
                Id = postId.ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "image",
                Categories = new List<Category>() { new Category { Id = Guid.NewGuid().ToString(), Name = "java" } },
                AuthorId = authorId.ToString(),
                AuthorEmail = "abcd@gmail.com",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now
            };

            this._mockReadPostRepository
                .Setup(x => x.GetByIdWithAuthorIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(postAggregateModel);

            var updateCommandHandler = new UpdatePostCommandHandler(this._mockWritePostRepository.Object, this._mockReadPostRepository.Object);

            var category = new CategoryDto
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".net"
            };

            var updateCommand = new UpdatePostCommand()
            {
                Id = postId.ToString(),
                Title = "new-title",
                Thumbnail = "new-image",
                Content = "new-content",
                Categories = new List<CategoryDto>() { category }
            };

            updateCommand.AddAuthorId(authorId.ToString());

            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;
            var actuals = await updateCommandHandler.Handle(updateCommand, token);
            actuals.Should().NotBeNull();
        }
    }
}
