﻿using Application.Command.Delete;
using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Repository;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.CommandTest
{
    public class DeletePostCommandHandler_Spec
    {
        private readonly Mock<IWritePostRepository<PostAggregateDataModel>> _mockWritePostRepository;
        private readonly Mock<IReadPostRepository<PostAggregateDataModel>> _mockReadPostRepository;
        public DeletePostCommandHandler_Spec()
        {
            this._mockWritePostRepository = new Mock<IWritePostRepository<PostAggregateDataModel>>();
            this._mockReadPostRepository = new Mock<IReadPostRepository<PostAggregateDataModel>>();
        }

        [Fact]
        public async Task DeleteCommandHander_Should_Handle_DeletePostCommand_Request()
        {
            this._mockWritePostRepository
                .Setup(x => x.DeleteAsync(It.IsAny<PostAggregateDataModel>(), It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            var postId = Guid.NewGuid();
            var authorId = Guid.NewGuid();

            var postAggregateModel = new PostAggregateDataModel()
            {
                Id = postId.ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "image",
                Categories = new List<Category>() { new Category { Id = Guid.NewGuid().ToString(), Name = "java" } },
                AuthorId = authorId.ToString(),
                AuthorEmail = "abcd@gmail.com",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now
            };

            this._mockReadPostRepository
                .Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(postAggregateModel);

            var deleteCommandHandler = new DeletePostCommandHandler(this._mockWritePostRepository.Object, this._mockReadPostRepository.Object);

            var deleteCommand = new DeletePostCommand()
            {
                Id = postId.ToString()
            };

            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;
            var actuals = await deleteCommandHandler.Handle(deleteCommand, token);
            actuals.Should().NotBeNull();
        }
    }
}
