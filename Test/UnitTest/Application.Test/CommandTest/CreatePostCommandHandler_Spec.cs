﻿using Application.Command.Create;
using Application.Dto;
using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Repository;
using MediatR;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.CommandTest
{
    public class CreatePostCommandHandler_Spec
    {
        private readonly Mock<IWritePostRepository<PostAggregateDataModel>> _mockWritePostRepository;
        private readonly Mock<IReadPostRepository<PostAggregateDataModel>> _mockReadPostRepository;
        public CreatePostCommandHandler_Spec()
        {
            this._mockWritePostRepository = new Mock<IWritePostRepository<PostAggregateDataModel>>();
            this._mockReadPostRepository = new Mock<IReadPostRepository<PostAggregateDataModel>>();
        }

        [Fact]
        public async Task CreatePostCommandHandler_Should_Handle_CreatePostCommand_Request()
        {
            this._mockWritePostRepository
                .Setup(x => x.AddAsync(It.IsAny<PostAggregateDataModel>(), It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            var createPostCommandHandler = new CreatePostCommandHandler(this._mockWritePostRepository.Object, this._mockReadPostRepository.Object);

            var category = new CategoryDto()
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".net"
            };

            var createPostCommand = new CreatePostCommand()
            {
                Title = "title",
                Thumbnail = "image",
                Content = "content",
                Categories = new List<CategoryDto>() { category }
            };

            var authorEmail = "abc@gmail.com";
            var authorId = Guid.NewGuid().ToString();
            createPostCommand.AddAuthorEmail(authorEmail);
            createPostCommand.AddAuthorId(authorId);

            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;
            var actuals = await createPostCommandHandler.Handle(createPostCommand, token);
            actuals.Should().NotBeNull();
        }
    }
}
