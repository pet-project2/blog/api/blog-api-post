﻿using Application.Query.Detail;
using Application.Query.List;
using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Repository;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.QueryTest
{
    public class ListPostQueryHandler_Spec
    {
        private readonly Mock<IReadPostRepository<PostAggregateDataModel>> _mockReadPostRepository;
        private readonly Mock<ILogger<ListPostQueryHandler>> _mockLogger;
        public ListPostQueryHandler_Spec()
        {
            this._mockReadPostRepository = new Mock<IReadPostRepository<PostAggregateDataModel>>();
            this._mockLogger = new Mock<ILogger<ListPostQueryHandler>>();
        }

        [Fact]
        public async Task ListQueryHandler_Should_Handle_ListPostQuery_Query_Then_Return_List_PostDto()
        {
            var postId = Guid.NewGuid();
            var authorId = Guid.NewGuid();

            var category = new Category { Id = Guid.NewGuid().ToString(), Name = "java" };
            var postAggregateModel = new PostAggregateDataModel()
            {
                Id = postId.ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "image",
                Categories = new List<Category>() { category },
                AuthorId = authorId.ToString(),
                AuthorEmail = "abcd@gmail.com",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now
            };

            var listPostAggregateModel = new List<PostAggregateDataModel>() { postAggregateModel };

            this._mockReadPostRepository
                .Setup(x => x.GetListByAuthorIdAsync(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync((0, listPostAggregateModel));

            var listPostQueryHandler = new ListPostQueryHandler(this._mockReadPostRepository.Object, this._mockLogger.Object);
            var listPostQuery = new ListPostQuery(authorId.ToString(), 0, 10);

            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;
            var actuals = await listPostQueryHandler.Handle(listPostQuery, token);

            actuals.Data.Lists.Should().HaveCount(1);
            actuals.Data.Lists.Should().SatisfyRespectively(first =>
            {
                first.Id.Should().Be(postAggregateModel.Id);
                first.Title.Should().Be(postAggregateModel.Title.ToString());
                first.Content.Should().Be(postAggregateModel.Content.ToString());
                first.Thumbnail.Should().Be(postAggregateModel.Thumbnail.ToString());
                first.Categories.Should().HaveCount(1);
                first.Categories.Should().SatisfyRespectively(firstCate =>
                {
                    firstCate.Id.Should().Be(category.Id);
                    firstCate.Name.Should().Be(category.Name);
                });

            });

        }
    }
}
