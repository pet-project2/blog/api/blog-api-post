﻿using Application.Dto;
using Application.Exceptions;
using Application.Query.Detail;
using FluentAssertions;
using Infrastructure.Models;
using Infrastructure.Repository;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.QueryTest
{
    public class DetailPostQueryHandler_Spec
    {
        private readonly Mock<IReadPostRepository<PostAggregateDataModel>> _mockReadPostRepository;
        private readonly Mock<ILogger<DetailPostQueryHandler>> _mockLogger;
        public DetailPostQueryHandler_Spec()
        {
            this._mockReadPostRepository = new Mock<IReadPostRepository<PostAggregateDataModel>>();
            this._mockLogger = new Mock<ILogger<DetailPostQueryHandler>>();
        }

        [Fact]
        public async Task DetailPostQueryHandler_Should_Handle_DetailPostQuery_Request_Then_Return_PostDto()
        {
            var postId = Guid.NewGuid();
            var authorId = Guid.NewGuid();

            var category = new Category { Id = Guid.NewGuid().ToString(), Name = "java" };
            var postAggregateModel = new PostAggregateDataModel()
            {
                Id = postId.ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "image",
                Categories = new List<Category>() { category },
                AuthorId = authorId.ToString(),
                AuthorEmail = "abcd@gmail.com",
                CreatedAt = DateTime.Now,
                LastModifiedAt = DateTime.Now
            };

            this._mockReadPostRepository
                .Setup(x => x.GetByIdWithAuthorIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(postAggregateModel);

            var detailQueryHandler = new DetailPostQueryHandler(this._mockReadPostRepository.Object, this._mockLogger.Object);

            var detailPostQuery = new DetailPostQuery()
            {
                PostId = postId.ToString(),
                AuthorId = authorId.ToString()
            };

            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;
            var postDto = await detailQueryHandler.Handle(detailPostQuery, token);

            //postDto.Should().NotBeNull();
            //postDto.Id.Should().Be(postAggregateModel.Id);
            //postDto.Title.Should().Be(postAggregateModel.Title);
            //postDto.Content.Should().Be(postAggregateModel.Content);
            //postDto.Thumbnail.Should().Be(postAggregateModel.Thumbnail);
            //postDto.Categories.Should().ContainSingle();
            //postDto.Categories.Should().SatisfyRespectively(
            //first => {
            //    first.Id.Should().Be(category.Id);
            //    first.Name.Should().Be(category.Name);
            //});
        }
    }
}
