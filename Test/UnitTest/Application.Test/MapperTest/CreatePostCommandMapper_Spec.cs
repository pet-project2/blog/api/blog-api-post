﻿using Application.Command.Create;
using Application.Dto;
using Application.Mapper;
using Domain;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.MapperTest
{
    public class CreatePostCommandMapper_Spec
    {
        [Fact]
        public void Create_Post_Command_Should_Mapping_To_Post_Aggregate()
        {
            var category = new CategoryDto()
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".net"
            };

            var createPostCommand = new CreatePostCommand()
            {
                Title = "title",
                Thumbnail = "image",
                Content = "content",
                Categories = new List<CategoryDto>() { category }
            };

            var authorEmail = "abc@gmail.com";
            var authorId = Guid.NewGuid().ToString();
            createPostCommand.AddAuthorEmail(authorEmail);
            createPostCommand.AddAuthorId(authorId);

            var post = ApplicationMapping.Mapper.Map<PostAggregate>(createPostCommand);

            post.Should().NotBeNull();
            post.Title.ToString().Should().Be(createPostCommand.Title);
            post.Thumbnail.ToString().Should().Be(createPostCommand.Thumbnail);
            post.Content.ToString().Should().Be(createPostCommand.Content);


            post.Categories.Should().ContainSingle();
            post.Categories.Should().SatisfyRespectively(first => {
                first.Id.Should().Be(category.Id);
                first.Name.Should().Be(category.Name);
            });
        }
    }
}
