﻿using Application.Dto;
using Application.Mapper;
using Domain;
using Domain.Events;
using FluentAssertions;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.MapperTest
{
    public class PostDtoMapper_Spec
    {
        [Fact]
        public void Post_Aggregate_Should_Mapping_To_Post_Dto()
        {
            var category = new Domain.Category(Guid.NewGuid(), ".NET");
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Domain.Category>() { category };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var postAggregate = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            var postDto = ApplicationMapping.Mapper.Map<PostDto>(postAggregate);

            postDto.Should().NotBeNull();
            postDto.Id.Should().Be(id.ToString());
            postDto.Title.ToString().Should().Be(postTitle);
            postDto.Content.ToString().Should().Be(postContent);
            postDto.Categories.Should().ContainSingle();
        }

        [Fact]
        public void Post_Aggregate_Data_Model_Should_Mapping_To_Post_Dto()
        {
            var category = new Infrastructure.Models.Category { Id = Guid.NewGuid().ToString(), Name = "category" };

            var postAggregateData = new PostAggregateDataModel()
            {
                Id = Guid.NewGuid().ToString(),
                Title = "title",
                Content = "content",
                Thumbnail = "http://localhost",
                AuthorEmail = "abc@gmail.com",
                AuthorId = Guid.NewGuid().ToString(),
                CreatedAt = DateTime.Now.ToUniversalTime(),
                LastModifiedAt = DateTime.Now.ToUniversalTime(),
                Categories = new List<Infrastructure.Models.Category>() { category }
            };

            var postDto = ApplicationMapping.Mapper.Map<PostDto>(postAggregateData);

            postDto.Should().NotBeNull();
            postDto.Id.Should().Be(postAggregateData.Id);
            postDto.Title.ToString().Should().Be(postAggregateData.Title);
            postDto.Content.ToString().Should().Be(postAggregateData.Content);
            postDto.Categories.Should().ContainSingle();
            postDto.Categories.Should().SatisfyRespectively(first => {
                first.Id.Should().Be(category.Id);
                first.Name.Should().Be(category.Name);
            });

        }
    }
}
