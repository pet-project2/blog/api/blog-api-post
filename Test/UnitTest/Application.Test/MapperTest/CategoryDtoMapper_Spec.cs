﻿using Application.Dto;
using Application.Mapper;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test.MapperTest
{
    public class CategoryDtoMapper_Spec
    {
        [Fact]
        public void CategoryDto_Should_Mapping_To_Domain_Category()
        {
            var categoryDto = new CategoryDto
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".NET"
            };

            var domainCategory = ApplicationMapping.Mapper.Map<Domain.Category>(categoryDto);
            domainCategory.Should().NotBeNull();
            domainCategory.Id.Should().Be(categoryDto.Id);
            domainCategory.Name.Should().Be(categoryDto.Name);
        }

        [Fact]
        public void Category_Should_Mapping_To_CategoryDto()
        {
            var domainCategory = new Domain.Category(Guid.NewGuid(), ".NET");

            var category = ApplicationMapping.Mapper.Map<CategoryDto>(domainCategory);
            category.Should().NotBeNull();
            category.Id.Should().Be(domainCategory.Id.ToString());
            category.Name.Should().Be(domainCategory.Name);
        }

        [Fact]
        public void Infrastructure_Category_Should_Mapping_To_CategoryDto()
        {
            var modelCategory = new Infrastructure.Models.Category()
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".NET"
            };

            var categoryDto = ApplicationMapping.Mapper.Map<CategoryDto>(modelCategory);
            categoryDto.Should().NotBeNull();
            categoryDto.Id.Should().Be(modelCategory.Id);
            categoryDto.Name.Should().Be(modelCategory.Name);
        }
    }
}
