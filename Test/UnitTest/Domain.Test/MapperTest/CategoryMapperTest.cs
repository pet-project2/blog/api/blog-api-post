﻿using Domain.IntegrationEvents;
using Domain.Mapper;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Domain.Test.MapperTest
{
    public class CategoryMapperTest
    {
        [Fact]
        public void Should_Mapping_From_Category_To_Category_Event()
        {
            var category = new Category(Guid.NewGuid(), ".NET Core");

            var categoryEvent = DomainMapping.Mapper.Map<CategoryEvent>(category);

            categoryEvent.Should().NotBeNull();
            categoryEvent.Id.Should().Be(category.Id.ToString());
            categoryEvent.Name.Should().Be(category.Name);
        }
    }
}
