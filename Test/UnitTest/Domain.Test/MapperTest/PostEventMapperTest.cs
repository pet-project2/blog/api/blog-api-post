﻿using Domain.Events;
using Domain.IntegrationEvents;
using Domain.Mapper;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Domain.Test.MapperTest
{
    public class PostEventMapperTest
    {
        [Fact]
        public void Should_Mapping_From_Post_Add_Event_To_Post_Integration_Event()
        {
            var category = new Category(Guid.NewGuid(), ".NET");

            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { category };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);

            var postAddEvent = new PostAddedEvent(post);

            var postMessagfe = DomainMapping.Mapper.Map<PostIntegrationEvent>(postAddEvent);

            postMessagfe.Should().NotBeNull();
            postMessagfe.Head.EventId.Should().NotBeNullOrEmpty();
            postMessagfe.Head.EvenType.Should().Be(nameof(PostAddedEvent));
            postMessagfe.Head.EventDate.Should().HaveDay(DateTime.Now.Day);
            postMessagfe.Head.EventDate.Should().HaveHour(DateTime.Now.Hour);

            postMessagfe.Body.Id.Should().Be(post.Id.ToString());
            postMessagfe.Body.Title.Should().Be(post.Title.ToString());
            postMessagfe.Body.Thumbnail.Should().Be(post.Thumbnail.ToString());
            postMessagfe.Body.Content.Should().Be(post.Content.ToString());
            postMessagfe.Body.AuthorEmail.Should().Be(post.Author.Email);
            postMessagfe.Body.AuthorId.Should().Be(post.Author.Id.ToString());
            postMessagfe.Body.Categories.Should().ContainSingle();
            postMessagfe.Body.Categories.Should().SatisfyRespectively(
                first =>
                {
                    first.Id.Should().Be(category.Id.ToString());
                    first.Name.Should().Be(category.Name);
                }
            );
        }
    }
}
