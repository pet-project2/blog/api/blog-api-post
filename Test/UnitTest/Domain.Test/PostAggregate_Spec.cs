﻿using Domain.Events;
using Domain.Exceptions;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Domain.Test
{
    public class PostAggregate_Spec
    {
        [Fact]
        public void Should_Create_Post_Aggregate_Object_With_Normal_Value()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            post.AddPostCreatedDomainEvent();

            post.Should().NotBeNull();
            post.Id.Should().Be(id);
            post.Title.ToString().Should().Be(postTitle);
            post.Content.ToString().Should().Be(postContent);
            post.Categories.Should().ContainSingle();
            post.Author.Email.Should().Be("abc@gmail.com");

            post.DomainEvents.Should().ContainSingle();
            post.DomainEvents.Should().SatisfyRespectively(first => {

                first.Should().BeOfType(typeof(PostAddedEvent));
            });
        }


        [Fact]
        public void Should_Throw_Exception_Post_Aggregate_Without_Post_Id()
        {
            var id = Guid.Empty;
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            Func<PostAggregate> act = () => new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            act.Should().Throw<ArgumentNullException>();
        }


        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Should_Throw_Exception_Post_Aggregate_When_Post_Title_Is_Null_Or_Empty(string postTitle)
        {
            var id = Guid.NewGuid();
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            Func<PostAggregate> act = () => new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            act.Should().Throw<PostAggregateException>().WithMessage("Post title can not be null or empty");
        }

        [Theory]
        [InlineData("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book")]
        public void Should_Throw_Exception_Post_Aggregate_When_Post_Title_Longer_Than_150_Character(string postTitle)
        {
            var id = Guid.NewGuid();
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            Func<PostAggregate> act = () => new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            act.Should().Throw<PostAggregateException>().WithMessage("Post title can not be longer than 150 character");
        }


        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Should_Throw_Exception_Post_Aggregate_When_Post_Image_Is_Null_Or_Empty(string imagePath)
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            Func<PostAggregate> act = () => new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            act.Should().Throw<PostAggregateException>().WithMessage("Post image can not be null or empty");
        }


        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Should_Not_Throw_Exception_Post_Aggregate_When_Post_Image_Is_Null_Or_Empty(string postContent)
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);

            post.Should().NotBeNull();
            post.Id.Should().Be(id);
            post.Title.ToString().Should().Be(postTitle);
            post.Content.ToString().Should().Be(postContent);
            post.Categories.Should().ContainSingle();
            post.Author.Email.Should().Be("abc@gmail.com");
        }

        [Fact]
        public void Should_Throw_Exception_Post_Aggregate_When_Post_Author_Is_Null()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var postContent = "post-Content";
            var imagePath = "http://localhost";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            Author author = null;

            Func<PostAggregate> act = () => new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            act.Should().Throw<PostAggregateException>().WithMessage("Post Author can not be null");
        }


        public static IEnumerable<object[]> CategoriesDataDump =>
            new List<object[]>
            {
                new object[] { null},
                new object[] { new List<Category>()},
            };

        [Theory]
        [MemberData(nameof(CategoriesDataDump))]
        public void Should_Throw_Exception_Post_Aggregate_When_Post_List_Category_Is_Null_Or_Empty(IEnumerable<Category> listCategory)
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var postContent = "post-Content";
            var imagePath = "http://localhost";
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            Func<PostAggregate> act = () => new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            act.Should().Throw<PostAggregateException>().WithMessage("Post Category can not null null or empty list");
        }


        [Fact]
        public void Should_Updated_Post_Title_With_Normal_Title()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);

            var titleUpdated = "updated";
            post.UpdateTitle(titleUpdated);

            post.Title.ToString().Should().Be(titleUpdated);
        }

        [Fact]
        public void Should_Updated_Post_Title_With_Normal_Thumnail()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);

            var imageUpdate = "Thumnail";
            post.UpdateThumnail(imageUpdate);

            post.Thumbnail.ToString().Should().Be(imageUpdate);
        }

        [Fact]
        public void Should_Updated_Post_Content_With_Normal_Content()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);

            var content = "new-content";
            post.UpdateContent(content);

            post.Content.ToString().Should().Be(content);
        }


        [Fact]
        public void Should_Update_List_Category_Post_Title_With_New_Category()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);

            var elementId = Guid.NewGuid();
            var elementId2 = Guid.NewGuid();
            var newListCategory = new List<Category>() { { new Category(elementId, ".Java") }, { new Category(elementId2, ".Golang") } };
            post.UpdateListCategories(newListCategory);

            post.Categories.Should().HaveCount(newListCategory.Count);
            post.Categories.Should().SatisfyRespectively(
                first =>
            {
                first.Id.Should().Be(elementId);
                first.Name.Should().Be(".Java");
            },
                second =>
            {
                second.Id.Should().Be(elementId2);
                second.Name.Should().Be(".Golang");
            }
            );
        }

        [Fact]
        public void Should_Update_Post_With_New_Normal_Value()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);

            // new content
            var newTitle = "new-title";
            var newImage = "new-image";
            var newContent = "new-content";
            var elementId = Guid.NewGuid();
            var elementId2 = Guid.NewGuid();
            var newListCategory = new List<Category>() { { new Category(elementId, ".Java") }, { new Category(elementId2, ".Golang") } };

            post.UpdatePost(newTitle, newImage, newContent, newListCategory);

            post.Title.ToString().Should().Be(newTitle);
            post.Thumbnail.ToString().Should().Be(newImage);
            post.Content.ToString().Should().Be(newContent);
            post.Categories.Should().HaveCount(newListCategory.Count);
            post.Categories.Should().SatisfyRespectively(
                first =>
                {
                    first.Id.Should().Be(elementId);
                    first.Name.Should().Be(".Java");
                },
                second =>
                {
                    second.Id.Should().Be(elementId2);
                    second.Name.Should().Be(".Golang");
                }
            );
        }

        [Fact]
        public void Should_Deleted_Post_And_Add_Delete_Post_Event_To_List_Event()
        {
            var id = Guid.NewGuid();
            var postTitle = "post-title";
            var imagePath = "http://localhost";
            var postContent = "post-content";
            var listCategory = new List<Category>() { { new Category(Guid.NewGuid(), ".NET") } };
            var author = new Author(Guid.NewGuid(), "abc@gmail.com");

            var post = new PostAggregate(id, postTitle, imagePath, postContent, listCategory, author);
            post.AddPostCreatedDomainEvent();
            post.AddPostDeletedDomainEvent();

            post.DomainEvents.Should().HaveCount(2);
            post.DomainEvents.Should().SatisfyRespectively(first =>
            {
                first.Should().BeOfType(typeof(PostAddedEvent));
            },
            second => {
                second.Should().BeOfType(typeof(PostDeletedEvent));
            });
        }
    }
}
