﻿using AutoMapper;
using Domain.Events;
using Domain.IntegrationEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Mapper
{
    public class PostEventMapper : Profile
    {
        public PostEventMapper()
        {
            CreateMap<PostAddedEvent, PostIntegrationEvent>()
                .ForPath(d => d.Body.Id, opt => opt.MapFrom(src => src.Post.Id))
                .ForPath(d => d.Body.Title, opt => opt.MapFrom(src => src.Post.Title))
                .ForPath(d => d.Body.Thumbnail, opt => opt.MapFrom(src => src.Post.Thumbnail))
                .ForPath(d => d.Body.Content, opt => opt.MapFrom(src => src.Post.Content))
                .ForPath(d => d.Body.Categories, opt => opt.MapFrom(src => src.Post.Categories))
                .ForPath(d => d.Body.AuthorId, opt => opt.MapFrom(src => src.Post.Author.Id))
                .ForPath(d => d.Body.AuthorEmail, opt => opt.MapFrom(src => src.Post.Author.Email))
                .ForPath(d => d.Head.EvenType, opt => opt.MapFrom(src => src.EventType));
        }
    }
}
