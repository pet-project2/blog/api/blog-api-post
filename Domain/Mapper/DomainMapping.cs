﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Mapper
{
    public class DomainMapping
    {
        public static IMapper Mapper => _lazy.Value;
        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<PostEventMapper>();
                cfg.AddProfile<CategoryMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });
    }
}
