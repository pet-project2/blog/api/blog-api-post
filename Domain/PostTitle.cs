﻿using Domain.Exceptions;
using Fury.Core.Models;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class PostTitle : ValueObject
    {
        private string _value { get; }
        internal PostTitle(string title)
        {
            if(string.IsNullOrEmpty(title))
            {
                throw new PostAggregateException("Post title can not be null or empty");
            }

            if(title.Length > 150)
            {
                throw new PostAggregateException("Post title can not be longer than 150 character");
            }

            this._value = title;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static PostTitle FromString(string title) => new PostTitle(title);

        public override string ToString()
        {
            return this._value;
        }
    }
}