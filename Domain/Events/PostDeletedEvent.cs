﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Events
{
    public class PostDeletedEvent : PostBaseEvent
    {
        public PostAggregate Post { get; set; }
        public PostDeletedEvent(PostAggregate post)
        {
            this.Post = post;
        }
        public override string EventType => nameof(PostDeletedEvent);
    }
}
