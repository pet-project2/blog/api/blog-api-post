﻿using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Events
{
    public class PostAddedEvent : PostBaseEvent
    {
        public PostAggregate Post { get; private set; }
        public PostAddedEvent(PostAggregate post)
        {
            this.Post = post;
        }

        public override string EventType => nameof(PostAddedEvent);
    }
}
