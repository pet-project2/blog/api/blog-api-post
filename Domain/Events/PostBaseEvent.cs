﻿using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Events
{
    public abstract class PostBaseEvent : BaseDomainEvent
    {
        public Guid EventId { get; private set; }
        public abstract string EventType { get;}
        public PostBaseEvent()
        {
            this.EventId = Guid.NewGuid();
        }
    }
}
