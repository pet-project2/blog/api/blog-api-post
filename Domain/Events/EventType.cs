﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Events
{
    public static class EventType
    {
        public const string CREATE = "C";
        public const string UPDATE = "U";
        public const string DELETE = "D";
    }
}
