﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Events
{
    public class PostUpdatedEvent : PostBaseEvent
    {
        public PostAggregate Post { get; private set; }
        public PostUpdatedEvent(PostAggregate post)
        {
            this.Post = post;
        }

        public override string EventType => nameof(PostUpdatedEvent);
    }
}
