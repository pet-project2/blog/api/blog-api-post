﻿using Fury.Core.Models;
using System.Collections.Generic;

namespace Domain
{
    public class PostContent : ValueObject
    {
        private readonly string _value;
        internal PostContent(string content) => this._value = content;
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static PostContent FromContent(string content) => new PostContent(content);

        public override string ToString()
        {
            return this._value;
        }
    }
}