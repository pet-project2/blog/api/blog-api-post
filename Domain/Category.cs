﻿using Fury.Core.Models;
using System;

namespace Domain
{
    public class Category : Entity
    {
        public string Name { get; private set; }
        private Category(Guid id) : base(id) { }

        public Category(Guid id, string name) : this(id)
        {
            this.Name = name;
        }
    }
}   