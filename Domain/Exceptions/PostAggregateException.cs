﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions
{
    public class PostAggregateException : Exception
    {
        public PostAggregateException(string message) : base(message)
        {

        }
    }
}
