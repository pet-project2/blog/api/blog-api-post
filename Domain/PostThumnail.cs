﻿using Domain.Exceptions;
using Fury.Core.Models;
using System.Collections.Generic;

namespace Domain
{
    public class PostThumnail : ValueObject
    {
        private readonly string _value;

        public static implicit operator string (PostThumnail thumnail) => thumnail._value;

        internal PostThumnail(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new PostAggregateException("Post image can not be null or empty");
            }
            this._value = value;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static PostThumnail FromPath(string path) => new PostThumnail(path);

        public override string ToString()
        {
            return this._value;
        }
    }
}