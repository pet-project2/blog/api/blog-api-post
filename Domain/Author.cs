﻿using Domain.Exceptions;
using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Author : Entity
    {
        public string Email { get; private set; }
        private Author(Guid id) : base(id) { }

        public Author(Guid id, string email) : this(id)
        {
            if (string.IsNullOrEmpty(email))
                throw new PostAggregateException("Post author's email can not be null or empty");

            this.Email = email;
        }
    }
}
