﻿using System.Collections.Generic;

namespace Domain.IntegrationEvents
{
    public class CategoryEvent
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    public class Body
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string AuthorId { get; set; }
        public string AuthorEmail { get; set; }
        public IEnumerable<CategoryEvent> Categories { get; set; }
    }
}
