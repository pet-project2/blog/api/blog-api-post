﻿using Fury.Core.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IntegrationEvents
{
    public class PostIntegrationEvent : BaseIntegrationEvent
    {
        public Header Head { get; set; }
        public Body Body { get; set; }
    }
}
