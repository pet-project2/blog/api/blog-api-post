﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IntegrationEvents
{
    public class Header
    {
        public string EventId { get; private set; } = Guid.NewGuid().ToString();
        public DateTime EventDate { get; private set; } = DateTime.Now;
        public string EvenType { get; set; }
    }
}
