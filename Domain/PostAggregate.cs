﻿using Domain.Events;
using Domain.Exceptions;
using Fury.Core.Interface;
using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class PostAggregate : Entity, IAggregateRoot
    {
        public PostTitle Title { get; private set; }
        public PostContent Content { get; private set; }
        public PostThumnail Thumbnail { get; private set; }
        public Author Author { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime LastModifiedAt { get; private set; }

        private readonly List<Category> _categories = new List<Category>();
        public IEnumerable<Category> Categories => _categories.AsReadOnly();
        public PostAggregate(Guid id) : base(id) { }

        public PostAggregate(Guid id, string postTitle, string imagePath, string content, IEnumerable<Category> categories, Author author) : this(id)
        {
            Title = PostTitle.FromString(postTitle);
            Thumbnail = PostThumnail.FromPath(imagePath);
            Content = PostContent.FromContent(content);

            CheckAuthorValid(author);
            Author = author;

            CheckCategories(categories);
            this._categories = categories.ToList();
        }

        public void AddPostCreatedDomainEvent()
        {
            var addedNewPostEvent = new PostAddedEvent(this);
            this.AddDomainEvent(addedNewPostEvent);
        }

        public void UpdateTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new PostAggregateException("Can not update post with title is null or empty");
            }

            Title = PostTitle.FromString(title);
        }

        public void UpdateThumnail (string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath))
            {
                throw new PostAggregateException("Can not update post with no thumnail");
            }

            Thumbnail = PostThumnail.FromPath(imagePath);
        }

        public void UpdateContent (string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                throw new PostAggregateException("Can not update post with content is null or empty");
            }

            Content = PostContent.FromContent(content);
        }

        public void UpdateListCategories(IEnumerable<Category> categories)
        {
            this.ClearListCategories();
            this.AddListCategories(categories);
        }

        public void UpdateCreateDate(DateTime dateTime)
        {
            this.CreatedAt = dateTime;
        }
        public void UpdateLastModified(DateTime dateTime)
        {
            this.LastModifiedAt = dateTime;
        }

        public void UpdatePost(string title, string imagePath, string content, IEnumerable<Category> categories)
        {
            this.UpdateTitle(title);
            this.UpdateThumnail(imagePath);
            this.UpdateContent(content);
            this.UpdateListCategories(categories);

            var updatedPostEvent = new PostUpdatedEvent(this);
            this.AddDomainEvent(updatedPostEvent);
        }

        public void AddPostDeletedDomainEvent()
        {
            var deletedDomainEvent = new PostDeletedEvent(this);
            this.AddDomainEvent(deletedDomainEvent);
        }

        private void ClearListCategories()
        {
            this._categories.Clear();
        }

        private void AddListCategories(IEnumerable<Category> categories)
        {
            this._categories.AddRange(categories);
        }

        private void CheckAuthorValid(Author author)
        {
            if (author == null)
                throw new PostAggregateException("Post Author can not be null");
        }

        private void CheckCategories(IEnumerable<Category> categories)
        {
            if (categories == null || !categories.Any())
                throw new PostAggregateException("Post Category can not null null or empty list");
        }
    }
}
 