﻿using Domain.Events;
using Domain.IntegrationEvents;
using Domain.Mapper;
using Fury.Core.Interface;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.EventHandler
{
    public class PostAddedEventHandler : INotificationHandler<PostAddedEvent>
    {
        private readonly IMessagePublisher<PostIntegrationEvent> _messagePublisher;
        private readonly ILogger<PostAddedEventHandler> _logger;
        public PostAddedEventHandler(IMessagePublisher<PostIntegrationEvent> messagePublisher, ILogger<PostAddedEventHandler> logger)
        {
            this._messagePublisher = messagePublisher;
            this._logger = logger;
        }
        public async Task Handle(PostAddedEvent postEvent, CancellationToken cancellationToken)
        {
            var postMessage = DomainMapping.Mapper.Map<PostIntegrationEvent>(postEvent);
            await this._messagePublisher.Publish(postMessage);
            this._logger.LogInformation($"Message published. {postEvent.Post.Title}");
        }
    }
}
