﻿using Api.Dto;
using Api.Exceptions;
using Application.Command.Create;
using Application.Command.Delete;
using Application.Command.Update;
using Application.Dto;
using Application.Query.Detail;
using Application.Query.List;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<PostController> _logger;
        public PostController(IMediator mediator, ILogger<PostController> logger)
        {
            this._mediator = mediator;
            this._logger = logger;
        }

        [Authorize]
        [ProducesResponseType(typeof(ApiQueryResponse<PostDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiErrorResponse), StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPostAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                var message = "Request failed cause: Post id is null or empty";
                this._logger.LogError(message);
                return BadRequest(message);
            }

            var principal = this.User;
            if (principal == null)
                throw new PostControllerException("Author is not authentication or not found");

            var authorId = principal.FindFirstValue(ClaimTypes.NameIdentifier);

            var query = new DetailPostQuery() { PostId = id, AuthorId = authorId };
            var result = await this._mediator.Send(query);

            var apiResponse = new ApiQueryResponse<PostDto>(result.Message,
                                                            StatusCodes.Status200OK,
                                                            result.Data);

            return Ok(apiResponse);
        }

        [Authorize]
        [ProducesResponseType(typeof(ApiQueryResponse<ListPostDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiErrorResponse), StatusCodes.Status500InternalServerError)]
        [HttpGet("list")]
        public async Task<IActionResult> GetListPostAsync([FromQuery] int pageNumber = 1, [FromQuery] int pageSize = 10)
        {
            var principal = this.User;
            if (principal == null)
            {
                var apiErrorResponse = new ApiErrorResponse("Author is not authentication or not found",
                                                            StatusCodes.Status401Unauthorized,
                                                            "permission_authentication",
                                                            "Principal user have not found");

                return StatusCode(StatusCodes.Status500InternalServerError, apiErrorResponse);
            }

            var authorId = principal.FindFirstValue(ClaimTypes.NameIdentifier);
            var query = new ListPostQuery(authorId, pageNumber, pageSize);

            var result = await this._mediator.Send(query);

            if (!result.IsSuccess)
            {
                var apiErrorResponse = new ApiErrorResponse(result.Message,
                                                            StatusCodes.Status500InternalServerError,
                                                            result.Failure.ErrorType,
                                                            result.Failure.Reason);

                return StatusCode(StatusCodes.Status500InternalServerError, apiErrorResponse);
            }


            var apiResponse = new ApiQueryResponse<ListPostDto>(result.Message,
                                                                StatusCodes.Status200OK,
                                                                result.Data);
            return Ok(apiResponse);
        }

        [Authorize]
        [HttpPost("create")]
        public async Task<IActionResult> CreatePostAsync(CreatePostCommand command)
        {
            try
            {
                if (command == null)
                    return BadRequest();

                var principal = this.User;
                if (principal == null)
                    throw new PostControllerException("Author is not authentication or not found");

                var authorId = principal.FindFirstValue(ClaimTypes.NameIdentifier);
                var authorEmail = principal.FindFirstValue(ClaimTypes.Email);

                command.AddAuthorId(authorId);
                command.AddAuthorEmail(authorEmail);

                await this._mediator.Send(command);
                return StatusCode(StatusCodes.Status201Created);

            }
            catch (Exception ex)
            {
                this._logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [Authorize]
        [HttpPut("update")]
        public async Task<IActionResult> UpdatePostAsync(UpdatePostCommand command)
        {
            try
            {
                if (command == null)
                    return BadRequest();

                var principal = this.User;
                if (principal == null)
                    throw new PostControllerException("Author is not authentication or not found");

                var authorId = principal.FindFirstValue(ClaimTypes.NameIdentifier);
                command.AddAuthorId(authorId);

                await this._mediator.Send(command);

                return Ok();
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [Authorize]
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeletePostAsync(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return BadRequest();

                await this._mediator.Send(new DeletePostCommand() { Id  = id});

                return Ok();
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
