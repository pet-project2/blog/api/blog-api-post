﻿using Infrastructure.IntegrationAppConfig;
using Infrastructure.Models;
using Infrastructure.Repository.SearchRepository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;

namespace Api.Extensions
{
    public static class ServiceElasticsearchExtensions
    {
        public static void AddElasticsearch(this IServiceCollection services, IConfiguration configuration)
        {
            // set config elasticsearch
            var esSetting = configuration.GetSection("Elasticsearch").Get<ElasticsearchConfig>();
            var settings = new ConnectionSettings(new Uri(esSetting.Uri))
                               .DefaultIndex(esSetting.Index)
                               .EnableDebugMode()
                               .MaximumRetries(1)
                               .MaxRetryTimeout(TimeSpan.FromSeconds(30))
                               .RequestTimeout(TimeSpan.FromMilliseconds(10));

            var client = new ElasticClient(settings);
            esSetting.AddPostMapping(client); // add post Mapping

            services.AddSingleton<IElasticClient>(client);
            services.AddScoped<ISearchRepository<PostAggregateDataModel>, SearchRepository>();
        }
    }
}
