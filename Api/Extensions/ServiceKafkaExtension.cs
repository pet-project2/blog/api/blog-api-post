﻿using Confluent.Kafka;
using Infrastructure.IntegrationAppConfig;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Extensions
{
    public static class ServiceKafkaExtensions
    {
        public static void AddKafkaProducer<TKey, TValue>(this IServiceCollection services, IConfiguration configuration)
        {
            

            var kafkaConfig = configuration.GetSection("Kafka");
            var kafkaSettings = kafkaConfig.Get<KafkaProducerConfig>();

            // add setting 
            services.Configure<KafkaProducerConfig>(kafkaConfig);

            var producerConfig = new ProducerConfig
            {
                BootstrapServers = kafkaSettings.Uri,
                Debug = "topic,msg"

            };

            var producerBuilder = new ProducerBuilder<TKey, TValue>(producerConfig).Build();
            services.AddSingleton(producerBuilder);
        }
    }
}
