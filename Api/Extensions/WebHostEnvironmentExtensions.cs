﻿using Api.Constants;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Extensions
{
    public static class WebHostEnvironmentExtensions
    {
        public static bool IsDevelopmenmt(this IWebHostEnvironment env)
        {
            return (env.EnvironmentName.ToLower() == EnviromentConst.DEVELOPMENT.ToLower());
        }
    }
}
