﻿using AutoMapper.Configuration;
using Domain.IntegrationEvents;
using Fury.Core.Interface;
using Infrastructure.MessageHandler;
using Infrastructure.Models;
using Infrastructure.Models.EfModels;
using Infrastructure.Repository;
using Infrastructure.Repository.EfRepository;
using Microsoft.Extensions.DependencyInjection;

namespace Api.Extensions
{
    public static class SerivceRegistryExtensions
    {
        public static void RegistryService(this IServiceCollection services)
        {
            services.AddScoped<IEfRepository<PostEf, PostAggregateDataModel>, EfRepository>();
            services.AddScoped<IWritePostRepository<PostAggregateDataModel>, WritePostRepository>();
            services.AddScoped<IReadPostRepository<PostAggregateDataModel>, ReadPostRepository>();
            services.AddScoped<IMessagePublisher<PostIntegrationEvent>, EventMessageHandler>();
        }
    }
}
