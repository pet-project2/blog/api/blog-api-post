﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Constants
{
    public struct EnviromentConst
    {
        public const string INTEGRATION_TEST = "Integration-Test";
        public const string DEVELOPMENT = "Development";
    }
}
