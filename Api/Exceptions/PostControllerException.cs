﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Exceptions
{
    public class PostControllerException : Exception
    {
        public PostControllerException(string message) : base(message)
        {

        }
    }
}
