﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dto
{
    public class Error
    {
        public string CauseBy { get; private set; }
        public string Reason { get; private set; }

        public Error(string errorType, string reason)
        {
            this.CauseBy = errorType;
            this.Reason = reason;
        }
    }
}
