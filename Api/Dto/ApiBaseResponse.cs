﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dto
{
    public abstract class ApiBaseResponse
    {
        public bool IsSuccess { get; private set; }
        public string Message { get; set; } = string.Empty;
        public int StatusCode { get; set; }
        public ApiBaseResponse(bool isSuccess)
        {
            this.IsSuccess = isSuccess;
        }
    }
}
