﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dto
{
    public class ApiQueryResponse<T> : ApiBaseResponse
    {
        public T Data { get; private set; }

        public ApiQueryResponse() : base(true)
        {

        }

        public ApiQueryResponse(string message, int statusCode, T result) : this()
        {
            this.Message = message;
            this.StatusCode = statusCode;
            this.Data = result;
        }
    }
}
