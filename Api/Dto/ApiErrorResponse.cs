﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dto
{
    public class ApiErrorResponse : ApiBaseResponse
    {
        public Error Error { get; private set; }
        public ApiErrorResponse() : base(false)
        {

        }

        public ApiErrorResponse(string message, int statusCodes) : this()
        {
            this.Message = message;
            this.StatusCode = statusCodes;
        }

        public ApiErrorResponse(string message, int statusCodes, string errorType, string reason) : this(message, statusCodes)
        {
            this.Error = new Error(errorType, reason);
        }
    }
}
