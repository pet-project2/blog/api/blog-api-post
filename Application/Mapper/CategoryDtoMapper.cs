﻿using Application.Dto;
using AutoMapper;
using Domain;
using System;

namespace Application.Mapper
{
    public class CategoryDtoMapper : Profile
    {
        public CategoryDtoMapper()
        {
            CreateMap<CategoryDto, Category>()
                .ConstructUsing(x => new Category(new Guid(x.Id), x.Name))
                .ForAllMembers(s => s.Ignore());

            CreateMap<Domain.Category, CategoryDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));

            // Map from Db => DTO
            CreateMap<Infrastructure.Models.Category, CategoryDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
