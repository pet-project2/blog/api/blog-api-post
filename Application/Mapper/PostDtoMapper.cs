﻿using Application.Dto;
using AutoMapper;
using Domain;
using Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace Application.Mapper
{
    public class PostDtoMapper : Profile
    {
        public PostDtoMapper()
        {
            CreateMap<PostAggregate, PostDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Title, s => s.MapFrom(src => src.Title))
                .ForMember(d => d.Thumbnail, s => s.MapFrom(src => src.Thumbnail))
                .ForMember(d => d.Content, s => s.MapFrom(src => src.Content))
                .ForMember(d => d.CreatedAt, s => s.MapFrom(src => src.CreatedAt))
                .ForMember(d => d.LastModifiedAt, s => s.MapFrom(src => src.LastModifiedAt))
                .ForMember(d => d.Categories, s => s.MapFrom(
                    src => ApplicationMapping.Mapper.Map<IEnumerable<CategoryDto>>(src.Categories.ToList())));

            CreateMap<PostAggregateDataModel, PostDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Title, s => s.MapFrom(src => src.Title))
                .ForMember(d => d.Thumbnail, s => s.MapFrom(src => src.Thumbnail))
                .ForMember(d => d.Content, s => s.MapFrom(src => src.Content))
                .ForMember(d => d.CreatedAt, s => s.MapFrom(src => src.CreatedAt))
                .ForMember(d => d.LastModifiedAt, s => s.MapFrom(src => src.LastModifiedAt))
                .ForMember(d => d.Categories, s => s.MapFrom(
                    src => ApplicationMapping.Mapper.Map<IEnumerable<CategoryDto>>(src.Categories.ToList())));
        }
    }
}
