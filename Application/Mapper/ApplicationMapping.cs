﻿using AutoMapper;
using System;

namespace Application.Mapper
{
    public class ApplicationMapping
    {
        public static IMapper Mapper => _lazy.Value;
        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CreatePostCommandMapper>();
                cfg.AddProfile<CategoryDtoMapper>();
                cfg.AddProfile<PostDtoMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });
    }
}
