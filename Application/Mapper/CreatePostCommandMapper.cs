﻿using Application.Command.Create;
using Application.Dto;
using AutoMapper;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mapper
{
    public class CreatePostCommandMapper : Profile
    {
        public CreatePostCommandMapper()
        {
            CreateMap<CreatePostCommand, PostAggregate>()
                .ConstructUsing(x => new PostAggregate(Guid.NewGuid(),
                x.Title,
                x.Thumbnail,
                x.Content,
                ApplicationMapping.Mapper.Map<List<CategoryDto>, List<Domain.Category>>(x.Categories), new Author(new Guid(x.AuthorId), x.AuthorEmail)))
                .ForAllMembers(s => s.Ignore());
        }
    }
}
