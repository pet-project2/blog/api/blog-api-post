﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Dto
{
    public class ListPostDto
    {
        public IEnumerable<PostDto> Lists { get; set; }
        public Paging Paging { get; set; }
    }
}
