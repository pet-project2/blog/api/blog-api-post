﻿namespace Application.Dto
{
    public class Result<T> : IResult<T>
    {
        public string Message { get; private set; }
        public Failure Failure { get; private set; }
        public T Data { get ; private set; }
        public bool IsSuccess { get; private set; }
        public static Result<T> SuccessData(string message, T value) => new() { IsSuccess = true, Data = value, Message = message};
        public static Result<T> Success(string message) => new() { IsSuccess = true, Message = message };
        public static Result<T> Failed(string message, Failure failure) => new() { IsSuccess = false, Message = message, Failure = failure};
    }
}
