﻿using Application.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Query.Detail
{
    public class DetailPostQuery : IRequest<IResult<PostDto>>
    {
        public string PostId { get; set; }
        public string AuthorId { get; set; }
    }
}
