﻿using Application.Dto;
using Application.Exceptions;
using Application.Mapper;
using Infrastructure.Models;
using Infrastructure.Repository;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Query.Detail
{
    public class DetailPostQueryHandler : IRequestHandler<DetailPostQuery, IResult<PostDto>>
    {
        private readonly IReadPostRepository<PostAggregateDataModel> _readRepository;
        private readonly ILogger<DetailPostQueryHandler> _logger;
        public DetailPostQueryHandler(IReadPostRepository<PostAggregateDataModel> readRepository, ILogger<DetailPostQueryHandler> logger)
        {
            this._readRepository = readRepository;
            this._logger = logger;
        }
        public async Task<IResult<PostDto>> Handle(DetailPostQuery request, CancellationToken cancellationToken)
        {
            var postId = request.PostId.ToLower().Trim();
            var authorId = request.AuthorId.ToLower().Trim();

            var post = await this._readRepository.GetByIdWithAuthorIdAsync(postId, authorId);
            if (post == null)
            {
                this._logger.LogInformation($"Could not found post with id: {postId}");
                return Result<PostDto>.Success($"Could not found post with id: {postId}");
            }
                
            var postDto = ApplicationMapping.Mapper.Map<PostDto>(post);
            var result = Result<PostDto>.SuccessData("Query post successfully", postDto);
            this._logger.LogInformation($"Founded post with id: {postId}");
            return result;
        }
    }
}
