﻿using Application.Dto;
using MediatR;
using System.Collections.Generic;

namespace Application.Query.List
{
    public class ListPostQuery : IRequest<IResult<ListPostDto>>
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public string AuthorId { get; private set; }

        public ListPostQuery(string authorId, int pageNumber, int pageSize)
        {
            this.AuthorId = authorId;
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 || pageSize <= 0 ? 10 : pageSize;
        }
    }
}
