﻿using Application.Dto;
using Application.Mapper;
using Infrastructure.Models;
using Infrastructure.Repository;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Query.List
{
    public class ListPostQueryHandler : IRequestHandler<ListPostQuery, IResult<ListPostDto>>
    {
        private readonly IReadPostRepository<PostAggregateDataModel> _readRepository;
        private readonly ILogger<ListPostQueryHandler> _logger;
        public ListPostQueryHandler(IReadPostRepository<PostAggregateDataModel> readRepository, ILogger<ListPostQueryHandler> logger)
        {
            this._readRepository = readRepository;
            this._logger = logger;
        }
        public async Task<IResult<ListPostDto>> Handle(ListPostQuery request, CancellationToken cancellationToken)
        {
            var authorId = request.AuthorId.Trim().ToLower();
            var limit = request.PageSize;
            var offset = limit * (request.PageNumber - 1);

            var listPost = await this._readRepository.GetListByAuthorIdAsync(authorId, offset, limit, cancellationToken);

            var listPostDto = ApplicationMapping.Mapper.Map<IEnumerable<PostDto>>(listPost.Item2);
            var paging = Paging.GetPaging(request.PageNumber, request.PageSize, listPost.Item1);
            var result = Result<ListPostDto>.SuccessData("Query list post successfully", new ListPostDto()
            {
                Lists = listPostDto,
                Paging = paging
            });

            return result;
        }
    }
}
