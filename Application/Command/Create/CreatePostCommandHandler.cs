﻿using Application.Dto;
using Application.Mapper;
using Domain;
using Infrastructure.Mapper;
using Infrastructure.Models;
using Infrastructure.Repository;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreatePostCommandHandler : IRequestHandler<CreatePostCommand, IResult<Unit>>
    {
        private readonly IWritePostRepository<PostAggregateDataModel> _writeRepository;
        private readonly IReadPostRepository<PostAggregateDataModel> _readPostRepository;

        public CreatePostCommandHandler(IWritePostRepository<PostAggregateDataModel> writeRepository, IReadPostRepository<PostAggregateDataModel> readPostRepository)
        {
            this._writeRepository = writeRepository;
            this._readPostRepository = readPostRepository;
        }
        public async Task<IResult<Unit>> Handle(CreatePostCommand request, CancellationToken cancellationToken)
        {
            try
            {
                // map reques -> PostAggregate -> handle logic
                var postAggregate = ApplicationMapping.Mapper.Map<PostAggregate>(request);

                postAggregate.AddPostCreatedDomainEvent();

                // after handle logic -> map back to post model to save in db
                var post = InfrastructureMapping.Mapper.Map<PostAggregateDataModel>(postAggregate);

                // save post
                await this._writeRepository.AddAsync(post);

                var result = Result<Unit>.Success($"Create new post successfully postId: {post.Id}");

                return await Task.FromResult(result);
            }
            catch (Exception ex)
            {
                var failure = new Failure(ex.GetType().Name, ex.Message);
                var failResult = Result<Unit>.Failed("Can not create new post cause something happened", failure);

                return await Task.FromResult(failResult);
            }
        }
    }
}
