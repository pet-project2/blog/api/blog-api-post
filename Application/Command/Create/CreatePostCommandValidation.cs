﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreatePostCommandValidation : AbstractValidator<CreatePostCommand>
    {
        public CreatePostCommandValidation()
        {
            RuleFor(x => x.Title)
                .NotNull().WithMessage("Title can not be null")
                .NotEmpty().WithMessage("Title can not be empty")
                .MaximumLength(150).WithMessage("Title 's length is only maximum 150 charaters");

            RuleFor(x => x.Thumbnail)
                .NotNull().WithMessage("Thumnail can not be null")
                .NotEmpty().WithMessage("Thumnail can not be empty");

            RuleFor(x => x.Content)
                .NotNull().WithMessage("Content can not be null")
                .NotEmpty().WithMessage("Content can not be empty");

            RuleForEach(x => x.Categories)
                .NotNull()
                .WithMessage("Post's Category can not be empty. Please select at least one");

            RuleFor(x => x.CreateAt)
                .NotNull()
                .WithMessage("Create At Date can not be null");

            RuleFor(x => x.AuthorEmail)
                .NotNull().WithMessage("Content can not be null")
                .NotEmpty().WithMessage("Content can not be empty");
        }
    }
}
