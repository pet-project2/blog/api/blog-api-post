﻿using Application.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreatePostCommand : IRequest<IResult<Unit>>
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string AuthorId { get; private set; }
        public string AuthorEmail { get; private set; }
        public DateTime CreateAt { get; private set; } = DateTime.Now.ToUniversalTime();
        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();

        public void AddAuthorId(string authorId)
        {
            this.AuthorId = authorId;
        }

        public void AddAuthorEmail(string authorEmail)
        {
            this.AuthorEmail = authorEmail;
        }
    }
}
