﻿using Application.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Command.Update
{
    public class UpdatePostCommand : IRequest<IResult<Unit>>
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string AuthorId { get; private set; }
        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();
        public void AddAuthorId(string authorId)
        {
            this.AuthorId = authorId;
        }
    }
}
