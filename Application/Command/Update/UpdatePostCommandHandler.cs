﻿using Application.Dto;
using Application.Mapper;
using Domain;
using Infrastructure.Mapper;
using Infrastructure.Models;
using Infrastructure.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Update
{
    public class UpdatePostCommandHandler : IRequestHandler<UpdatePostCommand, IResult<Unit>>
    {
        private readonly IWritePostRepository<PostAggregateDataModel> _writePostRepository;
        private readonly IReadPostRepository<PostAggregateDataModel> _readPostRepository;
        public UpdatePostCommandHandler(IWritePostRepository<PostAggregateDataModel> writePostRepository, IReadPostRepository<PostAggregateDataModel> readPostRepository)
        {
            this._writePostRepository = writePostRepository;
            this._readPostRepository = readPostRepository;
        }
        public async Task<IResult<Unit>> Handle(UpdatePostCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var postId = request.Id.ToLower().Trim();
                var oldPost = await this._readPostRepository.GetByIdWithAuthorIdAsync(postId, request.AuthorId, cancellationToken);
                if (oldPost == null)
                {
                    var noPostFoundResult = Result<Unit>.Success("No Post found to update");
                    return await Task.FromResult(noPostFoundResult);
                }

                var postAggregate = InfrastructureMapping.Mapper.Map<PostAggregate>(oldPost);
                var listDomainCategory = ApplicationMapping.Mapper.Map<IEnumerable<Domain.Category>>(request.Categories);

                postAggregate.UpdatePost(request.Title, request.Thumbnail, request.Content, listDomainCategory);
                var newPost = InfrastructureMapping.Mapper.Map<PostAggregateDataModel>(postAggregate);

                // save to db
                await this._writePostRepository.UpdateAsync(oldPost, newPost, cancellationToken);
                var result = Result<Unit>.Success($"Update post id : {oldPost.Id} successfully");

                return await Task.FromResult(result);
            }
            catch (Exception ex)
            {
                var failure = new Failure(ex.GetType().Name, ex.Message);
                var failResult = Result<Unit>.Failed("Can not delete post cause something happened", failure);
                return await Task.FromResult(failResult);
            }
        }
    }
}
