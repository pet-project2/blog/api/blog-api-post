﻿using Application.Dto;
using Infrastructure.Models;
using Infrastructure.Repository;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Delete
{
    public class DeletePostCommandHandler : IRequestHandler<DeletePostCommand, IResult<Unit>>
    {
        private readonly IWritePostRepository<PostAggregateDataModel> _writeRepository;
        private readonly IReadPostRepository<PostAggregateDataModel> _readPostRepository;
        public DeletePostCommandHandler(IWritePostRepository<PostAggregateDataModel> writeRepository, IReadPostRepository<PostAggregateDataModel> readPostRepository)
        {
            this._writeRepository = writeRepository;
            this._readPostRepository = readPostRepository;
        }
        public async Task<IResult<Unit>> Handle(DeletePostCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var postId = request.Id;
                var post = await this._readPostRepository.GetByIdAsync(postId);
                if(post == null)
                {
                    var noPostDeleteResult = Result<Unit>.Success($"No post found to delete");
                    return await Task.FromResult(noPostDeleteResult);
                }

                await this._writeRepository.DeleteAsync(post);
                var result = Result<Unit>.Success($"Delete post id: {post.Id} successfully");

                return await Task.FromResult(result);
            }
            catch (Exception ex)
            {
                var failure = new Failure(ex.GetType().Name, ex.Message);
                var failResult = Result<Unit>.Failed("Can not delete post cause something happened", failure);
                return await Task.FromResult(failResult);
            }
        }
    }
}
